//
//  ProfileController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 18/03/2021.
//

import UIKit
var usrlogin : User? = nil

class ProfileTabBarController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
///******************************************************************
    
    
    
    //MARK: Buttons Action
    // --SignIn Button Action--

    @IBAction func SignInBtn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //Settings Action
    @IBAction func SettingsBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "SettingsID") as! SettingsController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
///******************************************************************
    
    
    
    
    
}
