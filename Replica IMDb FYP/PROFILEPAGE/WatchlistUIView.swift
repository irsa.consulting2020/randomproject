//
//  WatchlistUIView.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 07/04/2021.
//

import UIKit
import DropDown

class WatchlistUIView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {


    
    @IBOutlet weak var WatchlistLabel: UILabel!
    @IBOutlet weak var watchlistbtn: UIButton!
    
    @IBOutlet weak var watchlistGenresbtn: UIButton!
    
    @IBOutlet weak var WatchlistCollectionView: UICollectionView!
    
    let dropDown = DropDown()
    
     var videoname = ["sabrina"]
    
    
    
    
    override func awakeFromNib() {
        
        WatchlistLabel.layer.cornerRadius = 10
        watchlistGenresbtn.layer.cornerRadius = 20
        watchlistbtn.layer.cornerRadius = 20
        watchlistbtn.clipsToBounds = true
        watchlistGenresbtn.clipsToBounds = true
        WatchlistLabel.clipsToBounds = true
        
        WatchlistCollectionView.delegate = self
        WatchlistCollectionView.dataSource = self
        
        let nibName = UINib(nibName: "Watchlist2CollectionCell", bundle:nil)
        WatchlistCollectionView.register(nibName, forCellWithReuseIdentifier: "collectionCell")
    }
    
    
    
    @IBAction func WatchlistTypeBtn(_ sender: Any) {
        
        dropDown.dataSource = ["All","Movies","TV-series"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
        
        
    }
    
    @IBAction func WatchlistGenresBtn(_ sender: Any) {
        
        dropDown.dataSource = ["All","Action","Adventure","Animation","Comedy","Crime","Drama","Fantasy","Horror","Thriller","Western"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
        
        
    }
  
    
 //--------------------------------------------------------//
    
    //MARK: CollectionView Function
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videoname.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = WatchlistCollectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! Watchlist2CollectionViewCell
        cell.WatchlistVideoNameLbl.text = videoname[indexPath.row]
        return cell
    }
    
    
}
