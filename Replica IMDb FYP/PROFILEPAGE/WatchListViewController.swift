//
//  WatchListViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 09/05/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class WatchListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, watchList {
    
    
    
    @IBOutlet weak var BeforeWatchlistView: UIView!
    @IBOutlet weak var AfterWatchlistView: UIView!
    @IBOutlet weak var WatchlistCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        WatchlistCollectionView.delegate = self
        WatchlistCollectionView.dataSource = self
        fetchWatchlistData()
        fetchApiData()
        //End Of ViewDidLoad
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchWatchlistData()
    }
//    override func viewDidAppear(_ animated: Bool) {
//        fetchWatchlistData()
//    }
    //MARK: API Working
    
    var movie = [Movie]()
    func fetchWatchlistData(){
        var id = 0
         movie.removeAll()
         if usrlogin != nil {
             id = usrlogin!.UId
         }
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/watchlist?id=\(id)&rid=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ [self] (response) in
            let response = response.data!
            let array = JSON(response).arrayValue
            self.movie.removeAll()
            for record in array{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.movie.append(movierecord)
               
            }
            self.watchlist()
            self.WatchlistCollectionView.reloadData()
        }
    }
    var movie1 = [Movie]()
    func fetchApiData(){
        movie1.removeAll()
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AllMovies"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.movie1.append(movierecord)
            }
        }
    }
    @IBAction func WatchlistBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        vc.movieDetail = movie1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
///*********************************************************************
    //MARK: CollectionView Working
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movie.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = WatchlistCollectionView.dequeueReusableCell(withReuseIdentifier: "WatchListCell", for: indexPath) as! WatchlistCollectionViewCell
        let data = movie[indexPath.row]
        cell.delegate = self
        cell.VideosName.text = data.Movie_Name
        cell.WatchlistRanking.text = "\(data.Movie_Rating)"
        cell.movieId = data.Movie_Id
        AF.request( data.Movie_Image, method: .get).response{ response in
          switch response.result {
           case .success(let responseData):
            cell.WatchlistImageView.image = UIImage(data: responseData!, scale:1)
           case .failure(let error):
               print("error--->",error)
           }
       }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = movie[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    func watchlist(){
        let c = movie.count//compactMap({ $0.Movie_Id }).count
        if c == 0 {
            BeforeWatchlistView.isHidden = false
            AfterWatchlistView.isHidden = true
        }else{
            if c != 0 {
                BeforeWatchlistView.isHidden = true
                AfterWatchlistView.isHidden = false
            }
        }
    }
    
    
    
    //End of Class
}
