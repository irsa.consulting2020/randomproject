//
//  FanFavouriteViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 07/04/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class FanFavouriteViewController: UIViewController, UITableViewDelegate, SeeAllTableView {
    func fetchWatchlistData() {
//        if let push = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as? HomeTabBarController {
//            push.fetchWatchlistData() = self
//        }
      //  self.delegate1?.fetchWatchlistData()
    }
    
    
    
    var movieDetail: [Movie]? = nil
   // var delegate1 : Refresh?
    func SeeAllBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: TableView Delagtion
    func SeeAllTableView(index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlay") as! VideoPlay2ViewController
        vc.MovieDetail = movieDetail![index]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
///************************************************************************

    @IBOutlet weak var FanfavouriteTableView: UITableView!
    @IBOutlet weak var FaNFavLbl: UILabel!
    var videoname = ["Godzilla vs. Kong","Zack Snyder's Justice League","WandaVision"]
    var images = ["Fate","Marriage&Mortage","ParadisePD"]
    var rating = ["6.7","8.2","8.1"]
///************************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        FanfavouriteTableView.delegate = self
        FanfavouriteTableView.dataSource = self
        self.FanfavouriteTableView.register(UINib(nibName: "FanFavouriteTableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        
    }
    
///************************************************************************

    
    
    
   //End Of Class
}
///************************************************************************
    //MARK: TableView Working
extension FanFavouriteViewController: UITabBarControllerDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.movieDetail!.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FanfavouriteTableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! FanFavouriteTableViewCell
//        cell.RatingView.text = rating[indexPath.row]
//        cell.VideoNameLbl.text = videoname[indexPath.row]
//        let imagev = UIImage(named: images[indexPath.row])
//        cell.VideImageView.image = imagev
        
        cell.delegate = self
        cell.index = indexPath.row
        var data = Movie()

            data = movieDetail![indexPath.row]
        cell.VideoNameLbl.text = data.Movie_Name
        cell.RatingView.text = "\(data.Movie_Rating)"
        cell.movieId = data.Movie_Id
        AF.request( data.Movie_Image, method: .get).response{ response in
          switch response.result {
           case .success(let responseData):
            cell.VideImageView.image = UIImage(data: responseData!, scale:1)

           case .failure(let error):
               print("error--->",error)
           }
       }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
    
    
}
