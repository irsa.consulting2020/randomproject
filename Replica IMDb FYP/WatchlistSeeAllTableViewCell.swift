//
//  WatchlistSeeAllTableViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 17/06/2021.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON

class WatchlistSeeAllTableViewCell: UITableViewCell {

    //WatchlistCell
    
    @IBOutlet weak var WImageView: UIImageView!
    @IBOutlet weak var RatingView: CosmosView!
    @IBOutlet weak var VideoName: UILabel!
    var movieId = 0
   // var delegate: WatchlistSeeAll?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
           // self.delegate?.fetchWatchlistData()
        }
    }
    
    @IBAction func WatchDleteBtn(_ sender: Any) {
        DeleteWatchlistData()
    }
    
    
    
    
    
    
}
protocol WatchlistSeeAll {
    func fetchWatchlistData()
}
