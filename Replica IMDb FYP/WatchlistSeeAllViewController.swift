//
//  WatchlistSeeAllViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 17/06/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class WatchlistSeeAllViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var movieDetail: [Movie]? = nil
    
    @IBOutlet weak var WatchListTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WatchListTableView.delegate = self
        WatchListTableView.dataSource = self
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movieDetail!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = WatchListTableView.dequeueReusableCell(withIdentifier: "WatchlistCell", for: indexPath) as! WatchlistSeeAllTableViewCell
        //cell.delegate = self
        
        let data = movieDetail![indexPath.row]
        cell.VideoName.text = data.Movie_Name
        cell.RatingView.text = "\(data.Movie_Rating)"
        cell.movieId = data.Movie_Id
        AF.request( data.Movie_Image, method: .get).response{ response in
          switch response.result {
           case .success(let responseData):
            cell.WImageView.image = UIImage(data: responseData!, scale:1)
           case .failure(let error):
               print("error--->",error)
           }
       }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = movieDetail![indexPath.row]
        WatchListTableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
