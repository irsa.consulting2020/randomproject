//
//  RatingController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 19/03/2021.
//

import UIKit
import  Cosmos
import Alamofire
import SwiftyJSON

var loginUser : User? = nil
var fromRating = false
class RatingController: UIViewController {

    var MovieId = 0
    @IBOutlet weak var RatingSubmitBtn: UIButton!
    @IBOutlet weak var VideoName: UILabel!
    @IBOutlet weak var RatingView: CosmosView!
    @IBOutlet weak var ReviewsTextArea: UITextView!
    var rvalue : Double = 0.0
    
///************************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        self.RatingView.didTouchCosmos = { [self]
            rating in
            print("Rating: \(rating)")
            self.rvalue = rating
        }
        
        // user login working for rating process
        let loginVc = storyboard?.instantiateViewController(identifier: "SignInID")
        as! SignInController
        
        if loginUser == nil {
            fromRating = true 
            //first need login.
            self.navigationController?.pushViewController(loginVc, animated: true)
            RatingSubmitBtn.isEnabled = false
            
        }
        else{
            if loginUser != nil{
                RatingSubmitBtn.isEnabled = true
            }
        }
        RatingSubmitBtn.layer.cornerRadius = 5
        LoadPreviousRating()
        
        
        
        //End of ViewDidLoad
        }
///************************************************************************
    //MARK: API Working
    
    var user = [User]()
    var feedback = [FeedBack]()
    
    func PostDataInApi(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/FeedBack/PostFeedback")!
        let para: [String:Any] = [
            "Movie_Id": MovieId,
            "UId": loginUser!.UId,
            "User_Comment": self.ReviewsTextArea.text!,
            "User_Rating": rvalue
        ]
        AF.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
            case .failure(_):
                print(Error.self)
            }
        }
    }
    func LoadPreviousRating(){
        //        var id = 0
        //       //  movie.removeAll()
        //         if usrlogin != nil {
        //             id = usrlogin!.UId
        //         }
                    
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/FeedBack/UserFeedback?id=\(MovieId)&uid=\(usrlogin!.UId)")!
            
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{ [self]
                (response) in
                switch response.result  {
                case .success(_):
                    let myresult = try? JSON(data: response.data!).dictionaryObject
                    ReviewsTextArea.text = myresult!["User_Comment"] as! String
                    RatingView.rating = myresult!["User_Rating"] as! Double
                case .failure(_):
                    print(Error.self)
                }
            }
        }
    
    
///************************************************************************
    @IBAction func RatingSubmitBtn(_ sender: Any) {
        
        PostDataInApi()
        
    }
     
}
