//
//  CastHyperLinkViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 09/04/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class CastHyperLinkViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var CastDetail: Movie_Cast? = nil
    
    @IBOutlet weak var CastNameLabel: UILabel!
    @IBOutlet weak var CastImage: UIImageView!
    @IBOutlet weak var CastDOB: UILabel!
    @IBOutlet weak var CastCity: UILabel!
    @IBOutlet weak var CastMoviesDone: UILabel!
    
    @IBOutlet weak var CastCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CastCollectionView.delegate = self
        CastCollectionView.dataSource = self
        
        fetchDataFromAPI()
        fetchCastApiData()
        //End of ViewDidLoad
    }
    
    func fetchDataFromAPI(){
        
        let cImage = try! Data(contentsOf: URL(string: (CastDetail?.Cast_Image)!)!)
        self.CastImage.image = UIImage(data: cImage)
        let cName = CastDetail?.Cast_Name
        self.CastNameLabel.text = cName
        let cDob = CastDetail?.Cast_DateofBirth
        self.CastDOB.text = cDob
        let cCity = CastDetail?.Cast_City
        self.CastCity.text = cCity
        
        
       let cMovieDone: Int = CastDetail!.Cast_MoviesDone
        
        self.CastMoviesDone.text = "Total Movies Done By The " + cName! + " is " + String(cMovieDone)
        
    }
   
    
    var Cast = [Cast_Picture]()
    func fetchCastApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/Castpictures?mid=\(CastDetail!.Movie_Id)&cid=\(CastDetail!.Cast_Id)"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let C_Id = record["C_Id"].intValue
                let Cast_Id = record["Cast_Id"].intValue
                let C_Pictures = record["C_Pictures"].stringValue
    
                let movierecord = Cast_Picture(C_Id: C_Id, Cast_Id: Cast_Id, Movie_Id: Movie_Id, C_Pictures: C_Pictures)

                self.Cast.append(movierecord)
            }
            self.CastCollectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Cast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CastCollectionView.dequeueReusableCell(withReuseIdentifier: "CastPictures", for: indexPath) as! CastPicturesCollectionViewCell
        let c = Cast[indexPath.row]
        AF.request(c.C_Pictures , method: .get).response{ response in
          switch response.result {
           case .success(let responseData):
            cell.picturesImageView.image = UIImage(data: responseData!, scale:1)
           case .failure(let error):
               print("error--->",error)
           }
       }
        return cell
    }
    
}
