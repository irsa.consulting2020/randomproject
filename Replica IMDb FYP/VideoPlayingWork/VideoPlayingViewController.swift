//
//  VideoPlayingViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 08/04/2021.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVKit
import AVFoundation

class VideoPlayingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, myCollectionDelegate {
    
    var MovieDetail  : Movie? = nil
    var userLogin : User? = nil
    
    
    @IBOutlet weak var PlayBtn: UIButton!
    @IBOutlet weak var VideoPlayerView: UIView!
    @IBOutlet weak var VideoName: UILabel!
    @IBOutlet weak var VideoImage: UIImageView!
    @IBOutlet weak var VideoGenres: UILabel!
    @IBOutlet weak var VideoDescription: UILabel!
    @IBOutlet weak var VideoRating: UILabel!
    @IBOutlet weak var UserReviews: UIButton!
    @IBOutlet weak var ProducerName: UILabel!
    @IBOutlet weak var DirectorName: UILabel!
    @IBOutlet weak var StoryWriterName: UILabel!
    @IBOutlet weak var ReleaseDate: UILabel!
    @IBOutlet weak var CastCollectionView: UICollectionView!
    
    
    //MARK: VariableDeclaration
    
    var player = AVPlayer()
    let avController = AVPlayerViewController()
    var castname = ["Kristen Stewart","Robert Pattison","Bella Throne"]
    var castiamges = ["kristenstewart","robertPattison","bellathrone"]
    
    
///************************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  self.navigationController?.isNavigationBarHidden = true
        CastCollectionView.delegate = self
        CastCollectionView.dataSource = self
        
        self.addDirectorGesture()
        self.addProducerGesture()
        self.addWriterGesture()
        
        fetchMovieData()
        fetchMovieCastApiData()
        fetchMovieDirectorApiData()
        fetchMovieProducerApiData()
        fetchMovieStorywriterApiData()
        fetchTypeapi()
        fetchGenresapi()
        videoplayer()
        
        //PlayPauseWorking()
        //END OF ViewDidLoad
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player.pause()
      //  player = nil
    }
///************************************************************************
    //MARK: Functions
    
    func videoplayer(){
        //        avController.showsPlaybackControls = true
        //        avController.view.frame = VideoPlayerView.bounds
        //        self.VideoPlayerView.addSubview(avController.view)
        //        avController.player = player
        //        avController.videoGravity = .resizeAspectFill
        
        let url = MovieDetail?.Movie_Path
        self.player = AVPlayer(url: URL(string: url!)!) // your video url
        let playerLayer = AVPlayerLayer(player: self.player)
        playerLayer.frame = self.VideoPlayerView.bounds
        playerLayer.masksToBounds = true
        playerLayer.contentsGravity = .resizeAspectFill
        self.VideoPlayerView.contentMode = .scaleAspectFill
        playerLayer.frame = CGRect(x: 0, y: 0, width: 428, height: 200)
        playerLayer.videoGravity = .resizeAspectFill
        self.view.layer.insertSublayer(playerLayer, at: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        self.VideoPlayerView.layer.addSublayer(playerLayer)
        self.player.play()
        player.seek(to: CMTime.zero)
        
        //avController.player = player
        //present(avController, animated: true)
        
        player.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
    }
    @objc func playerItemDidReachEnd(){
        player.seek(to: CMTime.zero)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.player.rate == 0 {
            print("Pause")
        }else {
            print("Play")
        }
    }
    
    @IBAction func PlayBtn(_ sender: Any) {
        
        if player.timeControlStatus == .playing {
            player.pause()
           // imagedisplay()
        }else{
            if player.timeControlStatus == .paused {
                player.play()
            }
        }
        
    }
    func imagedisplay(){
        let image = UIImageView()
        image.image = UIImage(systemName: "play.fill")
        let contentview = UIView()
        contentview.addSubview(image)
        image.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        image.tintColor = .white
        
    }
    
    func PlayPauseWorking(){
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.Play(_:)))
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.Pause(_:)))
        switch player.timeControlStatus {
        case .paused:
            tap1.numberOfTapsRequired = 1
            self.VideoPlayerView.isUserInteractionEnabled = true
            self.VideoPlayerView.addGestureRecognizer(tap1)

        case .playing:
            tap.numberOfTapsRequired = 1
            self.VideoPlayerView.isUserInteractionEnabled = true
            self.VideoPlayerView.addGestureRecognizer(tap)
        default:
            break
        }
       
    }
    
   @objc func Pause(_ tap: UITapGestureRecognizer){
    player.pause()
    print("Done")
    }
    @objc func Play(_ tap: UITapGestureRecognizer){
     player.play()
     print("Done")
     }
    func fetchMovieData(){
        
        let data = try! Data(contentsOf: URL(string: MovieDetail!.Movie_Image)!)
        self.VideoImage.image =  UIImage(data: data)
        let Mname = MovieDetail?.Movie_Name
        self.VideoName.text = Mname
        let Mdescrip = MovieDetail?.Movie_Description
        self.VideoDescription.text = Mdescrip
        let Mrating = MovieDetail?.Movie_Rating
        self.VideoRating.text = "   " + "\(Mrating!)"
        VideoRating.textAlignment = .left
        let Mreleasedate = MovieDetail?.Movie_ReleaseDate
        self.ReleaseDate.text = Mreleasedate
        
    }
    
///************************************************************************
    //MARK: API Working
    
    var movieCast = [Movie_Cast]()
    
    func fetchMovieCastApiData(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/MovieCast/AllMovieCast?id=\(MovieDetail!.Movie_Id)")!
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
       //     print(JsonArray)
            for record in JsonArray{
                let Cast_Id = record["Cast_Id"].intValue
                let Cast_Name = record["Cast_Name"].stringValue
                let Cast_Image = record["Cast_Image"].stringValue
                let Cast_DateofBirth = record["Cast_DateofBirth"].stringValue
                let Cast_City = record["Cast_City"].stringValue
                let Cast_MoviesDone = record["Cast_MoviesDone"].intValue
                
                let array = Movie_Cast(Cast_Id: Cast_Id,
                                       Movie_Id: self.MovieDetail!.Movie_Id,
                                       Cast_Name: Cast_Name,
                                       Cast_Image: Cast_Image,
                                       Cast_DateofBirth: Cast_DateofBirth,
                                       Cast_City: Cast_City,
                                       Cast_MoviesDone: Cast_MoviesDone)
                self.movieCast.append(array)
            }
            self.CastCollectionView.reloadData()
        }
    }
    
    var movieDirector = [Movie_Director]()
    
    func fetchMovieDirectorApiData(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/MovieDirector?id=\(MovieDetail!.Movie_Id)")!
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Director_Id = record["Director_Id"].intValue
                let Director_Name = record["Director_Name"].stringValue
                let Director_Image = record["Director_Image"].stringValue
                let Director_Description = record["Director_Description"].stringValue
                let Director_KnownFor = record["Director_KnownFor"].stringValue
                let Director_Award = record["Director_Award"].stringValue
                
                let array = Movie_Director(Director_Id: Director_Id,
                                           Movie_Id: self.MovieDetail!.Movie_Id,
                                           Director_Name: Director_Name,
                                           Director_Image: Director_Image,
                                           Director_Description: Director_Description,
                                           Director_KnownFor: Director_KnownFor,
                                           Director_Award: Director_Award)
                self.movieDirector.append(array)
            }
            self.DirectorName.text = self.movieDirector[0].Director_Name
        }
    }
    
    var movieProducer = [Movie_Producer]()
    
    func fetchMovieProducerApiData(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/MovieProducer?id=\(MovieDetail!.Movie_Id)")!
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
          //  print(JsonArray)
            for record in JsonArray{
                let Producer_Id = record["Producer_Id"].intValue
                let Producer_Name = record["Producer_Name"].stringValue
                let Producer_Image = record["Producer_Image"].stringValue
                let Producer_Desccription = record["Producer_Desccription"].stringValue
                let Producer_KnownFor = record["Producer_KnownFor"].stringValue
                let Producer_Award = record["Producer_Award"].stringValue
                
                let array = Movie_Producer(Producer_Id: Producer_Id,
                                           Movie_Id: self.MovieDetail!.Movie_Id,
                                           Producer_Name: Producer_Name,
                                           Producer_Image: Producer_Image,
                                           Producer_Desccription: Producer_Desccription,
                                           Producer_KnownFor: Producer_KnownFor,
                                           Producer_Award: Producer_Award)
                self.movieProducer.append(array)
            }
            self.ProducerName.text = self.movieProducer[0].Producer_Name
        }
    }
    
    var movieStorywriter = [Movie_Storywriter]()
    
    func fetchMovieStorywriterApiData(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/MovieWriter?id=\(MovieDetail!.Movie_Id)")!
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Storywriter_Id = record["Storywriter_Id"].intValue
                let Storywriter_Name = record["Storywriter_Name"].stringValue
                let Storywriter_Image = record["Storywriter_Image"].stringValue
                let Storywriter_Description = record["Storywriter_Description"].stringValue
                let Storywriter_KnownFor = record["Storywriter_KnownFor"].stringValue
                let Storywriter_Award = record["Storywriter_Award"].stringValue
                
                let array = Movie_Storywriter(Storywriter_Id: Storywriter_Id,
                                              Movie_Id: self.MovieDetail!.Movie_Id,
                                              Storywriter_Name: Storywriter_Name,
                                              Storywriter_Image: Storywriter_Image,
                                              Storywriter_Description: Storywriter_Description,
                                              Storywriter_KnownFor: Storywriter_KnownFor,
                                              Storywriter_Award: Storywriter_Award)
                self.movieStorywriter.append(array)
            }
            self.StoryWriterName.text = self.movieStorywriter[0].Storywriter_Name
        }
    }
    
    var category1 = [Genres]()
    var type = [Type]()
    
    func fetchGenresapi(){
        let url = "http://10.211.55.3/replicaimdbapi/api/Movie/MovieGenres?id=\(MovieDetail!.Movie_Id)"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
                print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
              //  print(myresult!)
                for i in myresult! {
                    let id = i["Genres_Id"].intValue
                    let genres = i["Genres_Name"].stringValue

                    self.category1.append(Genres(Genres_Id: id, Genres_Name: genres))
                    //print(self.category1)
                }
                break
            case .failure:
                print(myresponse.error!)
                break
            }
            self.VideoGenres.text = self.category1[0].Genres_Name
        }
    }
    func fetchTypeapi(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/MovieType?id=\(MovieDetail!.Movie_Id)"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
            //    print(myresponse)
                print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
            //    print(myresult!)
                for i in myresult! {
                    let id = i["Type_Id"].intValue
                    let type = i["Type_Name"].stringValue

                    self.type.append(Type(Type_Id: id, Type_Name: type))
                   // print(self.category1)
                }
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    
    
///************************************************************************
    //MARK: Hyperlink Gestures
    
    func addProducerGesture() {

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.labelProducerTapped(_:)))
        tap.numberOfTapsRequired = 1
        self.ProducerName.isUserInteractionEnabled = true
        self.ProducerName.addGestureRecognizer(tap)
        ProducerName.textColor = .systemBlue
        
    }
    func addDirectorGesture() {

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.labelDirectorTapped(_:)))
        tap.numberOfTapsRequired = 1
        self.DirectorName.isUserInteractionEnabled = true
        self.DirectorName.addGestureRecognizer(tap)
        DirectorName.textColor = .systemBlue
    }
    func addWriterGesture() {

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.labelWriterTapped(_:)))
        tap.numberOfTapsRequired = 1
        self.StoryWriterName.isUserInteractionEnabled = true
        self.StoryWriterName.addGestureRecognizer(tap)
        StoryWriterName.textColor = .systemBlue
    }
    @objc func labelProducerTapped(_ tap: UITapGestureRecognizer) {
        navigateToNext(type: "P")
        }
    @objc func labelDirectorTapped(_ tap: UITapGestureRecognizer) {
        navigateToNext(type: "D")
        }
    @objc func labelWriterTapped(_ tap: UITapGestureRecognizer) {
        navigateToNext(type: "W")
        }
    func navigateToNext(type:String)
    {
        let vc = storyboard?.instantiateViewController(identifier: "ProductionHyperlink") as! ProductionHyperlinkViewController
        vc.callType = type
        //vc.MovieId = (MovieDetail?.Movie_Id)!
        if type == "P"{
        vc.ProducerDetail = self.movieProducer[0]
        }else
            if type == "D"{
                vc.DirectorDetail = self.movieDirector[0]
            }
        else
            if type == "W"{
                vc.WriterDetail = self.movieStorywriter[0]
        }
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
///************************************************************************
    //MARK: RatingActions
    
    @IBAction func RateThis(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Rating") as! RatingController
        vc.MovieId = MovieDetail!.Movie_Id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func UserReviews(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Reviews") as! UserReviewsViewController
        vc.MovieId = MovieDetail!.Movie_Id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
///************************************************************************
    //MARK: Cast Collectionview
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieCast.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CastCollectionView.dequeueReusableCell(withReuseIdentifier: "CastCell", for: indexPath as IndexPath) as! CastCollectionViewCell
//        cell.CastName.text = castname[indexPath.row]
//        let image = UIImage(named: castiamges[indexPath.row])
//        cell.CastImage.image = image
        
        let CastDetail = self.movieCast[indexPath.row]
        cell.delegate = self
        cell.index = indexPath.row
        let cimage = try! Data(contentsOf: URL(string: CastDetail.Cast_Image)!)
        cell.CastImage.image = UIImage(data: cimage)
        let cname = CastDetail.Cast_Name
        cell.CastName.text = cname
        
        return cell
         
    }
    func myCollectionDelegate(cindex: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "CastHyperlink") as! CastHyperLinkViewController
        vc.CastDetail = movieCast[cindex]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
}
