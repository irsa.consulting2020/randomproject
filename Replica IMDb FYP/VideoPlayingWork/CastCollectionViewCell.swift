//
//  CastCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 08/04/2021.
//

import UIKit

class CastCollectionViewCell: UICollectionViewCell {
    
    var index = 0
    @IBOutlet weak var CastImage: UIImageView!
    @IBOutlet weak var CastName: UILabel!
    var delegate: myCollectionDelegate?

    
    override func awakeFromNib() {
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CastCollectionViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.myCollectionDelegate(cindex: index)
        }
}
protocol myCollectionDelegate {
    func myCollectionDelegate(cindex: Int)
}
