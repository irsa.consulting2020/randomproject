//
//  ProductionHyperlinkViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 09/04/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductionHyperlinkViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    

    var ProducerDetail : Movie_Producer? = nil
    var WriterDetail : Movie_Storywriter? = nil
    var DirectorDetail : Movie_Director? = nil
    
    var MovieId  = 0
    var callType = "" // P,D,W
    
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var DescriptionLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var KnownForLbl: UILabel!
    @IBOutlet weak var AwardsLbl: UILabel!
    @IBOutlet weak var PicturesCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        PicturesCollectionView.delegate = self
        PicturesCollectionView.dataSource = self
        
        fetchDataFromAPI()
        if callType == "P" {
        fetchProductionApiData()
        }
        else if callType == "D" {
            fetchDirectorApiData()
        }else if callType == "W"{
            fetchWriterApiData()
            
        }
        // End Of ViewDidLoad
    }
    
    func fetchDataFromAPI(){
       // var subUrl = ""
        if callType == "P"{
           // subUrl = "/Movie/MovieProducer?id=\(MovieId)"

            //ProducerDetail
            NameLbl.text = ProducerDetail?.Producer_Name
            DescriptionLbl.text = ProducerDetail?.Producer_Desccription
            let data = try! Data(contentsOf: URL(string: ProducerDetail!.Producer_Image)!)
            self.imageView.image =  UIImage(data: data)
            KnownForLbl.text = ProducerDetail?.Producer_KnownFor
            AwardsLbl.text = ProducerDetail?.Producer_Award
        }
        else if callType == "D"{
           // subUrl = "/Movie/MovieDirector?id=\(MovieId)"
        
            NameLbl.text = DirectorDetail?.Director_Name
            DescriptionLbl.text = DirectorDetail?.Director_Description
            let data = try! Data(contentsOf: URL(string: DirectorDetail!.Director_Image)!)
            self.imageView.image =  UIImage(data: data)
            KnownForLbl.text = DirectorDetail?.Director_KnownFor
            AwardsLbl.text = DirectorDetail?.Director_Award
        }
        else if callType == "W"{
          //  subUrl = "/Movie/MovieWriter?id=\(MovieId)"
            
            NameLbl.text = WriterDetail?.Storywriter_Name
            DescriptionLbl.text = WriterDetail?.Storywriter_Description
            let data = try! Data(contentsOf: URL(string: WriterDetail!.Storywriter_Image)!)
            self.imageView.image =  UIImage(data: data)
            KnownForLbl.text = WriterDetail?.Storywriter_KnownFor
            AwardsLbl.text = WriterDetail?.Storywriter_Award
        }
        
        
    }
    
    
    
    var production = [Producer_Picture]()
    func fetchProductionApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/Producerpictures?mid=\(ProducerDetail!.Movie_Id)&pid=\(ProducerDetail!.Producer_Id)"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let P_Id = record["P_Id"].intValue
                let Producer_Id = record["Producer_Id"].intValue
                let Producer_Pictures1 = record["Producer_Pictures1"].stringValue
    
                let movierecord = Producer_Picture(P_Id: P_Id, Producer_Id: Producer_Id, Movie_Id: Movie_Id, Producer_Pictures1: Producer_Pictures1)

                self.production.append(movierecord)
            }
            self.PicturesCollectionView.reloadData()
            
        }
    }
    var Director = [Director_Picture]()
    func fetchDirectorApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/Directorpictures?mid=\(DirectorDetail!.Movie_Id)&did=\(DirectorDetail!.Director_Id)"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let D_Id = record["D_Id"].intValue
                let Director_Id = record["Director_Id"].intValue
                let Pictures = record["Pictures"].stringValue
    
                let movierecord = Director_Picture(D_Id: D_Id, Director_Id: Director_Id, Movie_Id: Movie_Id, Pictures: Pictures)

                self.Director.append(movierecord)
            }
            self.PicturesCollectionView.reloadData()
            
        }
    }
    var Writer = [StoryWriter_Picture]()
    func fetchWriterApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/Writerpictures?mid=\(WriterDetail!.Movie_Id)&wid=\(WriterDetail!.Storywriter_Id)"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let S_Id = record["S_Id"].intValue
                let Storywriter_Id = record["Storywriter_Id"].intValue
                let S_Pictures = record["S_Pictures"].stringValue
    
                let movierecord = StoryWriter_Picture(S_Id: S_Id, Storywriter_Id: Storywriter_Id, Movie_Id: Movie_Id, S_Pictures: S_Pictures)

                self.Writer.append(movierecord)
            }
            self.PicturesCollectionView.reloadData()
            
        }
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if callType == "P" {
            return production.count
        }else if callType == "D" {
            return Director.count
        }else if callType == "W" {
            return Writer.count
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = PicturesCollectionView.dequeueReusableCell(withReuseIdentifier: "DirectorMoviePictures", for: indexPath) as! ProductionPicturesCollectionViewCell
        var c = Producer_Picture()
        
        if callType == "P"{
             c = production[indexPath.row]
            AF.request(c.Producer_Pictures1 , method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell.PicturesimageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
            return cell
        }
        else if callType == "D"{
           let  c = Director[indexPath.row]
            AF.request(c.Pictures , method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell.PicturesimageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
            return cell
        }
        else if callType == "W"{
            let c = Writer[indexPath.row]
            AF.request(c.S_Pictures , method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell.PicturesimageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
            return cell
        }
        
//        AF.request(c.Producer_Pictures1 , method: .get).response{ response in
//          switch response.result {
//           case .success(let responseData):
//            cell.PicturesimageView.image = UIImage(data: responseData!, scale:1)
//           case .failure(let error):
//               print("error--->",error)
//           }
//       }
        return cell
    }
  
}
