//
//  ComingSoonVideoPlayingViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 16/06/2021.
//

import UIKit
import AVFoundation
import AVKit


class ComingSoonVideoPlayingViewController: UIViewController {

    var MovieDetail  : Movie? = nil
    var userLogin : User? = nil
    
    
    @IBOutlet weak var VideoView: UIView!
    @IBOutlet weak var VideoDescription: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var VideoName: UILabel!
    @IBOutlet weak var PlayButton: UIButton!
    
    
    var player = AVPlayer()
    let avController = AVPlayerViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchMovieData()
        videoplayer()
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player.pause()
      
    }
    ///************************************************************************
        //MARK: Functions
        func videoplayer(){
            let url = MovieDetail?.Movie_Path
            
    //        avController.showsPlaybackControls = true
    //        avController.view.frame = VideoPlayerView.bounds
    //        self.VideoPlayerView.addSubview(avController.view)
    //        avController.player = player
    //        avController.videoGravity = .resizeAspectFill
            
            
            
            self.player = AVPlayer(url: URL(string: url!)!) // your video url
            let playerLayer = AVPlayerLayer(player: self.player)
            playerLayer.frame = self.VideoView.bounds
            playerLayer.masksToBounds = true
            playerLayer.contentsGravity = .resizeAspectFill
            self.VideoView.contentMode = .scaleAspectFill
            playerLayer.frame = CGRect(x: 0, y: 0, width: 428, height: 200)
            playerLayer.videoGravity = .resizeAspectFill
            self.view.layer.insertSublayer(playerLayer, at: 0)
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
            self.VideoView.layer.addSublayer(playerLayer)
            self.player.play()
            player.seek(to: CMTime.zero)
            
            //avController.player = player
            //present(avController, animated: true)
            
            player.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        }
        @objc func playerItemDidReachEnd(){
            player.seek(to: CMTime.zero)
        }
        
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if self.player.rate == 0 {
                print("Pause")
            }else {
                print("Play")
            }
        }

    @IBAction func PlayingButton(_ sender: Any) {
        if player.timeControlStatus == .playing {
            player.pause()
           // imagedisplay()
        }else{
            if player.timeControlStatus == .paused {
                player.play()
            }
        }
        
    }
    
    func fetchMovieData(){
        
        let data = try! Data(contentsOf: URL(string: MovieDetail!.Movie_Image)!)
        self.ImageView.image =  UIImage(data: data)
        let Mname = MovieDetail?.Movie_Name
        self.VideoName.text = Mname
        let Mdescrip = MovieDetail?.Movie_Description
        self.VideoDescription.text = Mdescrip
       
        
    }
    
}
