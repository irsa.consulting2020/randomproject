//
//  UserReviewsViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 08/04/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class UserReviewsViewController: UIViewController, UITableViewDelegate {

    
    var MovieId = 0
    
    
    @IBOutlet weak var ReviewsTableView: UITableView!
    
    var userreviews = ["osm","fab","lit","gorgeious","amazing","scary"]
    var username = ["Hammad","Ahad","Laraib","fan","Abyar","MK"]
    
    
///************************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        ReviewsTableView.delegate = self
        ReviewsTableView.dataSource = self
        self.ReviewsTableView.register(UINib(nibName: "UserReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsCell")
        
        
        
        FetchUserReviewsApiData()
        FetchUserApiData()
        
        // End of ViewDidLoad
    }
///************************************************************************
    //MARK: API Working
    var feedback = [FeedBack]()
    func FetchUserReviewsApiData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/FeedBack/Feedback?id=\(MovieId)")!
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
       //     print(JsonArray)
            for record in JsonArray{
                let FId = record["FId"].intValue
                let UId = record["UId"].intValue
                let Movie_Id = record["Movie_Id"].intValue
                let User_Comment = record["User_Comment"].stringValue
                let User_Rating = record["User_Rating"].floatValue
                
                let array = FeedBack(FId: FId,
                                     UId: UId,
                                     Movie_Id: Movie_Id,
                                     User_Comment: User_Comment,
                                     User_Rating: User_Rating)
                
                self.feedback.append(array)
            }
            self.ReviewsTableView.reloadData()
        }
    }
    
    var user = [User]()    
    
    func FetchUserApiData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/User/UserLoggedIn?id=\(MovieId)")!
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
            //print(JsonArray)
            for record in JsonArray{

                let UId = record["UId"].intValue
                let UName = record["UName"].stringValue
                let UEmail = record["UEmail"].stringValue
                let UPassword = record["UPassword"].stringValue
                let UType = record["UType"].stringValue
                
                let array = User(UId: UId,
                                 UName: UName,
                                 UEmail: UEmail,
                                 UPassword: UPassword,
                                 UType: UType)
                
                self.user.append(array)
            }
            self.ReviewsTableView.reloadData()
        }
    }
///************************************************************************
    
    
   
   // End Of Class
}
///************************************************************************


    //MARK: Extensions
extension UserReviewsViewController: UITabBarControllerDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.userreviews.count
        return self.feedback.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  ReviewsTableView.dequeueReusableCell(withIdentifier: "ReviewsCell", for: indexPath) as! UserReviewsTableViewCell
        
        let user1 = user[indexPath.row]
        let feedBack = feedback[indexPath.row]
        
//        cell.UserNameLbl.text = username[indexPath.row]
//        cell.userReviews.text = userreviews[indexPath.row]
        
        cell.UserNameLbl.text = user1.UName
        cell.userReviews.text = feedBack.User_Comment
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
    
}
