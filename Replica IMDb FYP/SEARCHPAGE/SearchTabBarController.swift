//
//  SearchController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 20/03/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class SearchTabBarController: UIViewController, UISearchBarDelegate, UISearchResultsUpdating, UITableViewDelegate, SearchDelegate  {
    
    
    func SearchActionBtn() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: TableView Delegation
    func SearchDelegate(Index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        if isSearching == false {
            vc.MovieDetail = movie[Index]
        }else{
            vc.MovieDetail = Searchedmovie[Index]
        }
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
///************************************************************************
    
    @IBOutlet weak var SearchTableView: UITableView!
    var filteredData : [String]!
    var searchBarPlaceholder: UIView!
    var searchController = UISearchController(searchResultsController: nil)
    var isSearching = false
    
///************************************************************************
        override func viewDidLoad() {
           super.viewDidLoad()
            SearchTableView.delegate = self
            SearchTableView.dataSource = self
            self.SearchTableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
            Search()
            fetchTableViewApiData()
            
            
            
            filteredData = data
            searchController.searchResultsUpdater = self
          //  searchBarPlaceholder.addSubview(searchController.searchBar)
            automaticallyAdjustsScrollViewInsets = false
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.sizeToFit()
            definesPresentationContext = true
            
            
            
            
        }
    
///**********************************************************************
    //MARK: SearchBar Working
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //isSearching = false
        SearchTableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        SearchTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)

//        filteredData = searchText.isEmpty ? data : data.filter { (item: String) -> Bool in
//            return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
//        }
        
        fetchSearchData(queryText: searchText)
        SearchTableView.reloadData()
    }
    func updateSearchResults(for searchController: UISearchController) {
        print("Called")
    }
    
    func Search(){
        
        let search = UISearchController(searchResultsController: nil)
        self.navigationItem.searchController = search
        navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController!.searchBar.delegate = self
        self.navigationItem.searchController!.searchResultsUpdater = self
        self.navigationItem.searchController?.searchBar.showsCancelButton = true
        self.navigationItem.searchController?.searchBar.placeholder = "Search IMDb"
        self.navigationItem.searchController?.searchBar.barTintColor = .yellow
        self.navigationItem.searchController?.searchBar.barStyle = .black
        self.navigationItem.searchController?.searchBar.autocapitalizationType = .words
        self.navigationItem.searchController?.searchBar.autocorrectionType = .yes
        
       // self.navigationItem.titleView = searchController.searchBar
     //   self.definesPresentationContext = true
    
    }
    
    
    
///*********************************************************************
    //MARK: API Working
    var data = [String]()
    
    var movie = [Movie]()
    func fetchTableViewApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AllMovies"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue
                
                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)
                                        
                self.movie.append(movierecord)
                self.data.append(Movie_Name)
            }
            self.SearchTableView.reloadData()
        }
    }
    var Searchedmovie = [Movie]()
    
    func fetchSearchData(queryText:String){
        
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/GetCustomSearch?lookfor=\(queryText)"
        AF.request(url.replacingOccurrences(of: " ", with: "%20"), method: .get, parameters: nil).responseJSON{
                   (response) in
            
            self.Searchedmovie.removeAll()
            if response.data == nil {
                self.SearchTableView.reloadData()
                return
            }
            let response1 = response.data!
            let jsonArray = JSON(response1).arrayValue
            
            for record in jsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue
                
                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name, 
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)
                                        
                self.Searchedmovie.append(movierecord)
            }
            self.SearchTableView.reloadData()
        }
    }
    
    
    
    //End of Class
}
///*********************************************************************
    //MARK: TableView Working

extension SearchTabBarController: UITabBarControllerDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching == true {
            return self.Searchedmovie.count
        }
        else{
            if isSearching == false {
                return self.movie.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SearchTableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchTableViewCell
        cell.delegate = self
        cell.index = indexPath.row
        var data = Movie()
        if isSearching == true {
            data = Searchedmovie[indexPath.row]
        }
        else{
          data =   movie[indexPath.row]
        }
        cell.Name.text = data.Movie_Name
        cell.Rating.text = "\(data.Movie_Rating)"
        cell.movieId = data.Movie_Id
        AF.request( data.Movie_Image, method: .get).response{ response in
          switch response.result {
           case .success(let responseData):
            cell.ImageView.image = UIImage(data: responseData!, scale:1)
           case .failure(let error):
               print("error--->",error)
           }
       }
        return cell
    }
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        print("searched")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
    
}
