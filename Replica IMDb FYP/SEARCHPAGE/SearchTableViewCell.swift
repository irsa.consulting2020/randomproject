//
//  SearchTableViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 30/04/2021.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Rating: CosmosView!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var Addbtn: UIButton!
    @IBOutlet weak var AddTag: UIImageView!
    var index = 0
    var delegate: SearchDelegate?
    var movieId = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchTableViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
               
    }
    ///************************************************************
//MARK: API Working
    
    var watch = [Watchlist]()
    
    func InsertDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)&rId=\(RoleId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        ]
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            
        }
    }
    
    
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.SearchDelegate(Index: index)
        }
    @objc private func btnAction(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.SearchActionBtn()
        }
    
    @IBAction func AddWatchlist(_ sender: Any) {
        if usrlogin != nil {
        if Addbtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            Addbtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            Addbtn.tintColor = .black
            Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            Addbtn.tag = 2
            print(Addbtn.tag)
            InsertDataInApi()
        }else{
            if Addbtn.tag == 2 {
               
                AddTag.tintColor = .black
                Addbtn.setImage(UIImage(systemName: "plus"), for: .normal)
                Addbtn.tintColor = .white
                Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
               
                Addbtn.tag = 0
                print(Addbtn.tag)
                DeleteWatchlistData()
            }
        }
      }else{
        Addbtn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
      }
    }
    
    
    
    
}
protocol SearchDelegate {
    func SearchDelegate(Index: Int)
    func SearchActionBtn()
}
