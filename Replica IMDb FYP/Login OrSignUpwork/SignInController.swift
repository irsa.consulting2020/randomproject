//
//  SignInController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 18/03/2021.
//

import UIKit

class SignInController: UIViewController {
    
    
    var fadingLabel: UILabel!

    @IBOutlet weak var SignInBtn1: UIButton!
    @IBOutlet weak var CreateAccountBtn1: UIButton!
    @IBOutlet weak var SignInEmailtxtfield: UITextField!{
        didSet{
           
          SignInEmailtxtfield.setLeftView(image: UIImage.init(named: "email1")!)
            SignInEmailtxtfield.tintColor = .darkGray
            
        }
     }
    
    @IBOutlet weak var SignInPasswordTxtfield: UITextField!{
        didSet{
           
          SignInPasswordTxtfield.setLeftView(image: UIImage.init(named: "password")!)
            SignInPasswordTxtfield.tintColor = .darkGray
            SignInPasswordTxtfield.isSecureTextEntry = true
        }
     }
    
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var ImageLogo: UIImageView!
    @IBOutlet weak var SignInView: UIView!
    
    var iconClick = false
    var imageIcon = UIImageView()
    
    
///************************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        CreateAccountBtn1.layer.cornerRadius = 5
        SignInBtn1.layer.cornerRadius = 5
        ImageLogo.layer.cornerRadius = 10
        //SignInView.layer.cornerRadius = 20
        
        
        let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blurredView.frame = self.view.bounds
        blurredView.alpha = 0.5
        blurredView.backgroundColor = .black
        view.addSubview(blurredView)
        view.bringSubviewToFront(SignInView)
        SignInView.backgroundColor = .none
        
        self.PasswordTextField.isSecureTextEntry = true
        ImageToggle()
     
        
    
        // Fading Label Working
   fadingLabel = UILabel()
   fadingLabel.text = "Text"
   fadingLabel.textColor = .black
   view.addSubview(fadingLabel)
   fadingLabel.isHidden = true
   fadingLabel.translatesAutoresizingMaskIntoConstraints = false
   fadingLabel.topAnchor.constraint    (equalTo: view.topAnchor,   constant:     60).isActive = true
   fadingLabel.leftAnchor.constraint   (equalTo: view.leftAnchor,  constant:     100).isActive = true
   fadingLabel.widthAnchor.constraint  (equalTo: view.widthAnchor, multiplier:  0.7).isActive = true
   fadingLabel.heightAnchor.constraint (equalToConstant: 30).isActive = true
        
        
    }
///************************************************************************
    //MARK: Buttons Actions
    //CreateAccount Button Action
    @IBAction func CreateAccountBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "CreateAccountID") as! CreateAccountController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // --SignIn Button Action
    @IBAction func SignInBtn(_ sender: Any) {
        // --CODE HERE--
        let user = User()
                let Email: String? =  EmailTextField.text!
                let Pass: String? = PasswordTextField.text!
        user.UEmail = Email!
        user.UPassword = Pass!
                    let db = APIWrapper()
                    let result = db.getMethodCall(controllerName: "User", actionName: "UserLogin?email=\(user.UEmail)&password=\(user.UPassword)")
                    
                    if result.ResponseCode == 200{
                        print("Done")
                        let decoder = JSONDecoder()
                        loginUser = try! decoder.decode(User.self, from: result.ResponseData!)
                        usrlogin =  try! decoder.decode(User.self, from: result.ResponseData!)
                        if fromRating {
                            self.navigationController?.popViewController(animated: true)
                        }
                        else if !DirectComing {
                            if root != nil {
                            root?.updateTabs()
                            }
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        else{
                        let vc = storyboard?.instantiateViewController(withIdentifier: "CustomTabBar")
                        self.navigationController?.pushViewController(vc!, animated: true)
                            fadeMessage(message: "Login Successful!", color: .black, finalAlpha: 0.0)
                        }
                    }else{
                        print(result.ResponseMessage)
                        displayErrorMessage(message: "Incorrect Email or Password")
                    }
            }
        
///************************************************************************
    //MARK: Functions
    func displayErrorMessage(message:String) {
           let alertView = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
           let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
           }
           alertView.addAction(OKAction)
           if let presenter = alertView.popoverPresentationController {
               presenter.sourceView = self.view
               presenter.sourceRect = self.view.bounds
           }
           self.present(alertView, animated: true, completion:nil)
       }
        
        
    fileprivate func ImageToggle()  {

        /// --Toggle Image--

        imageIcon.image = UIImage(named: "eyeslash")
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.frame = CGRect(x: -10, y: 0, width: 20, height: 20)

        imageIcon.frame = CGRect(x: -10, y: 0, width: 20, height: 20)

        PasswordTextField.rightView = contentView
        PasswordTextField.rightViewMode = .always

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)

    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){

        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if iconClick{

            iconClick = false
            tappedImage.image = UIImage(named: "eye")
            PasswordTextField.isSecureTextEntry = false
        }
        else{
            iconClick = true
            tappedImage.image = UIImage(named: "eyeslash")
            PasswordTextField.isSecureTextEntry = true
        }

    }
    ///***********************************************************
    //MARK: Fading Label
    func fadeMessage(message: String, color: UIColor, finalAlpha: CGFloat) {
        fadingLabel.text          = message
        fadingLabel.textColor = .black
        fadingLabel.alpha         = 1.0
        fadingLabel.isHidden      = false
        fadingLabel.textAlignment = .center
        fadingLabel.backgroundColor     = .white
        fadingLabel.layer.cornerRadius  = 5
        fadingLabel.layer.masksToBounds = true
        fadingLabel.font = .boldSystemFont(ofSize: 12)
        UIView.animate(withDuration: 3.0, animations: { () -> Void in
            self.fadingLabel.alpha = finalAlpha
        })
    }

    
    
    
    
    
    
// End Of Class
}
extension UITextField {
  func setLeftView(image: UIImage) {
    let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25)) // set your Own size
    iconView.image = image
    let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
    iconContainerView.addSubview(iconView)
    leftView = iconContainerView
    leftViewMode = .always
    self.tintColor = .lightGray
  }
}
