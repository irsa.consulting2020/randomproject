//
//  UploadImageViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 31/03/2021.
//

import UIKit
import DropDown
import AVKit
import Alamofire
import SwiftyJSON

class UploadImageViewController: UIViewController {
    
 //MARK: Variable Declaration

    let dropDown = DropDown()
    var player = AVPlayer()
    var imagePicker: ImagePicker!
    var videoPicker: VideoPicker!
    
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var DateAndTime: UIDatePicker!
    @IBOutlet weak var UploadedImageView: UIImageView!
    @IBOutlet weak var SelectimageBtn: UIButton!
    @IBOutlet weak var UploadBtn: UIButton!
    @IBOutlet weak var CancelBtn: UIButton!
    
    
    
///***********************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        UploadBtn.layer.cornerRadius = 5
        SelectimageBtn.layer.cornerRadius = 5
        CancelBtn.layer.cornerRadius = 5
        UploadedImageView.layer.cornerRadius = 5
       
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
     //   DropDown.appearance().selectedTextColor = UIColor.systemBlue
        
//        self.videoView.repeat = .loop
        
        
        
        
//        let url = "http://10.211.55.3/replicaimdbapi/api/"
//        uploadVideo(videoUrl: URL(string: url)!)
        
        
        
        
        
    }
    
///***********************************************************************
    //MARK: DropDown Buttons
    
    
    @IBAction func TypeDropDownBTN(_ sender: Any) {
        
        dropDown.dataSource = ["All","Movies","TV-series"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
        
    }
    
    
    @IBAction func GenresDropDown(_ sender: Any) {
        
        dropDown.dataSource = ["All","Action","Adventure","Animation","Comedy","Crime","Drama","Fantasy","Horror","Thriller","Western"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
        
        
    }
    
    
///***********************************************************************
    //MARK: Selction File FRom Gallery
    
    
    @IBAction func SelectImage(_ sender: Any) {
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
               self.imagePicker.present(from: sender as! UIView)
    }
    

    
    @IBAction func SelectVideo(_ sender: Any) {
        self.videoPicker = VideoPicker(presentationController: self, delegate: self)
        self.videoPicker.present(from: sender as! UIView)
        
    }
    
   
    @IBAction func CancelBTN(_ sender: Any) {
        
        UploadedImageView.image = nil
        videoView.layer.sublayers?.removeLast()
    }
    
    
    
///***********************************************************************
    //MARK: Upload Button
    
    
    @IBAction func UploadBtn(_ sender: Any) {
        

        let image: UIImage = UploadedImageView.image!
        let imagedata = image.pngData()
        let db = APIWrapper()
        let result = db.UploadImageMethodCall(cJson: imagedata!, endPoint: "Videos/UploadFile")


        if result.ResponseCode == 200 {
            print("Done")
        }
        else{
                print(result.ResponseMessage)

        }
        
        
        // Need to convert videourl into data then it can be uploaded in to the server...!!!
        
       // let data = try Data(contentsOf: videoUrl, options: .mappedIfSafe)
       //                 print(data)
        
       // let url = "http://10.211.55.3/replicaimdbapi/api/Videos/UploadFile"
        
       // uploadVideo(videoUrl: URL(string: url)!)
        
       //END OF upload btn
  }
    

    func uploadVideo(videoUrl: URL) { // local video file path..
           let timestamp = NSDate().timeIntervalSince1970 // just for some random name.
        
        AF.upload(multipartFormData: {(MultipartFormData) in MultipartFormData.append(videoUrl, withName: "Image", fileName: "\(timestamp).mp4", mimeType: "\(timestamp)/mp4")}, to: "Videos/UploadFile").responseJSON{
            (response) in
            debugPrint(response)
        }
        
        
       }
    
    
    
    
}

///***********************************************************************
    //MARK: Extensions for uploading

extension UploadImageViewController: ImagePickerDelegate {

    func didSelect(image: UIImage?) {
        guard let image = image else {
            return
        }
        self.UploadedImageView.image = image
    }
}
extension UploadImageViewController: VideoPickerDelegate {
    
    func didSelect(url: URL?) {
        guard let url = url else {
            return
        }
        
        self.player = AVPlayer(url: url) // your video url
        let playerLayer = AVPlayerLayer(player: self.player)
              playerLayer.frame = videoView.bounds
        self.videoView.contentMode = .scaleAspectFill
              videoView.layer.addSublayer(playerLayer)

        self.player.play()
        self.player.isMuted = true
      //  self.player.pause()
       // DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//        self.videoView.url = url
//        self.videoView.player?.play()
      //  self.videoView.setup(url: url)
       // }
    }
}






    
  
    
  
