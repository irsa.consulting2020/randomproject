//
//  AdminLoginController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 18/03/2021.
//

import UIKit

class AdminLoginController: UIViewController {

    
    @IBOutlet weak var AdminLogo: UIImageView!
    @IBOutlet weak var AdminEmailTextField: UITextField!{
        didSet{
           
            AdminEmailTextField.setLeftView(image: UIImage.init(named: "email1")!)
            AdminEmailTextField.tintColor = .darkGray
          
        }
     }
    @IBOutlet weak var AdminPasswordTextField: UITextField!{
        didSet{
           
            AdminPasswordTextField.setLeftView(image: UIImage.init(named: "password")!)
            AdminPasswordTextField.tintColor = .darkGray
            AdminPasswordTextField.isSecureTextEntry = true
        }
     }
    @IBOutlet weak var SignInBtn: UIButton!
    @IBOutlet weak var AdminView: UIView!
    
    var iconClick = false
    var imageIcon = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SignInBtn.layer.cornerRadius = 5
        AdminLogo.layer.cornerRadius = 10
        //AdminView.layer.cornerRadius = 20

        AdminPasswordTextField.isSecureTextEntry = true
        ImageToggle()
        
        let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blurredView.frame = self.view.bounds
        blurredView.alpha = 0.5
        blurredView.backgroundColor = .black
        view.addSubview(blurredView)
        view.bringSubviewToFront(AdminView)
        AdminView.backgroundColor = .none
        
        
        
    }
    

    @IBAction func AdminSignInBtn(_ sender: Any) {
        
        let user = User()
                
                let Email: String? =  AdminEmailTextField.text!
                
                let Pass: String? = AdminPasswordTextField.text!
        user.UEmail = Email!
        user.UPassword = Pass!
                    let db = APIWrapper()
                    let result = db.getMethodCall(controllerName: "User", actionName: "UserLogin?email=\(user.UEmail)&password=\(user.UPassword)")
      //  if user.UType == "Admin"{
                    
                    if result.ResponseCode == 200{
                        print("Done")
                        let vc = storyboard?.instantiateViewController(withIdentifier: "AdminHomeView")
                        self.navigationController?.pushViewController(vc!, animated: true)
                        //self.present(vc!, animated: true)
                    }else{
                        print(result.ResponseMessage)
                        displayErrorMessage(message: "Incorrect Email or Password")
                    }
//        }else{
//            displayErrorMessage(message: "Please Enter Correct Credentials")
//        }
    }
    
    func displayErrorMessage(message:String) {
           let alertView = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
           let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
           }
           alertView.addAction(OKAction)
           if let presenter = alertView.popoverPresentationController {
               presenter.sourceView = self.view
               presenter.sourceRect = self.view.bounds
           }
           self.present(alertView, animated: true, completion:nil)
       }
        
        
    fileprivate func ImageToggle()  {

        /// --Toggle Image--

        imageIcon.image = UIImage(named: "eyeslash")
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.frame = CGRect(x: -10, y: 0, width: 20, height: 20)

        imageIcon.frame = CGRect(x: -10, y: 0, width: 20, height: 20)

        AdminPasswordTextField.rightView = contentView
        AdminPasswordTextField.rightViewMode = .always

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)

    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){

        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if iconClick{

            iconClick = false
            tappedImage.image = UIImage(named: "eye")
            AdminPasswordTextField.isSecureTextEntry = false
        }
        else{
            iconClick = true
            tappedImage.image = UIImage(named: "eyeslash")
            AdminPasswordTextField.isSecureTextEntry = true
        }

    }

    
    
   

}
