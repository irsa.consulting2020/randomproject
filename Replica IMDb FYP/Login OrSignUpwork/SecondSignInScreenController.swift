//
//  SecondSignInScreenController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 18/03/2021.
//

import UIKit

class SecondSignInScreenController: UIViewController {

    
    @IBOutlet weak var CreateBtn: UIButton!
    @IBOutlet weak var SignInBtn: UIButton!
    @IBOutlet weak var ConditionOfUse: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        CreateBtn.layer.cornerRadius = 5.0
        SignInBtn.layer.cornerRadius = 5.0
        
        
        
    }
    
    @IBAction func CreateAccountBtn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "CreateAccountID") as! CreateAccountController
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    
    @IBAction func SignInBtn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "SignInID") as! SignInController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
