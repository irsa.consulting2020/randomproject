//
//  ViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 07/03/2021.
//
import AVKit
import UIKit
import AVFoundation

class ViewController: UIViewController{
    
    

    @IBOutlet weak var ConditionOfUse: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var faceBookBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var amazonBtn: UIButton!
    @IBOutlet weak var imdbBtnSignIn: UIButton!
    @IBOutlet weak var NotNowBtn: UIButton!
    @IBOutlet weak var CreateAccountBtn: UIButton!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        logoImage.layer.cornerRadius = 7.0
        imdbBtnSignIn.layer.cornerRadius = 5
        CreateAccountBtn.layer.cornerRadius = 5
        NotNowBtn.layer.cornerRadius = 5
        
        self.addGesture()

    }
    // -- Actions --
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
        tap.numberOfTapsRequired = 1
        self.ConditionOfUse.isUserInteractionEnabled = true
        self.ConditionOfUse.addGestureRecognizer(tap)
    }

    @objc
       func labelTapped(_ tap: UITapGestureRecognizer) {

        let vc = storyboard?.instantiateViewController(identifier: "ConditionOfUseImdb") as! ConditionOfUseIMDBController
        self.navigationController?.pushViewController(vc, animated: true)
       }
    // --Not Now Button Action
    
    @IBAction func NotNowBtn(_ sender: Any) {
        DirectComing = false
        let vc = storyboard?.instantiateViewController(identifier: "CustomTabBar") as! CustomUITabBarController
        self.navigationController?.pushViewController(vc, animated: true)
       // self.present(vc, animated: true)
    }
    
    
    
    
    
    // --SignIn Button--
    
    @IBAction func SignInBtn(_ sender: Any) {
        DirectComing = true 
        let vc = storyboard?.instantiateViewController(identifier: "SignInID") as! SignInController
        self.navigationController?.pushViewController(vc, animated: true)
       // self.present(vc, animated: true)
    }
    
    
    // --Create Account Button--
    
    @IBAction func CreateAccountBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "CreateAccountID") as! CreateAccountController
        self.navigationController?.pushViewController(vc, animated: true)
       // self.present(vc, animated: true)
        
    }
    
    
    // --Admin Login Button Action--
    
    
    @IBAction func AdminLoginBtn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "AdminLogin") as! AdminLoginController
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true)
    }
    
    
    
}



