//
//  CreateAccountController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 18/03/2021.
//

import UIKit
import AVKit
import Alamofire
import SwiftyJSON

class CreateAccountController: UIViewController{

    @IBOutlet weak var CopyRightLabel: UILabel!
    @IBOutlet weak var PrivacyNoticeLabel: UILabel!
    @IBOutlet weak var ConditionOfUseLabel: UILabel!
    @IBOutlet weak var AlreadyAccountLabel: UILabel!
    @IBOutlet weak var Password8CharacterLabel: UILabel!
    
    @IBOutlet weak var PasswordTextField: UITextField!{
        didSet{
           
            PasswordTextField.setLeftView(image: UIImage.init(named: "password")!)
            PasswordTextField.tintColor = .darkGray
            PasswordTextField.isSecureTextEntry = true
            PasswordTextField.autocorrectionType = .no
          //  PasswordTextField.textContentType = .oneTimeCode
            PasswordTextField.textContentType = .newPassword
        }
     }
    @IBOutlet weak var EmailTextField: UITextField!{
        didSet{
           
            EmailTextField.setLeftView(image: UIImage.init(named: "email1")!)
            EmailTextField.tintColor = .darkGray
            EmailTextField.autocorrectionType = .no
        }
     }
    @IBOutlet weak var userNameTxtField: UITextField!{
        didSet{
           
            userNameTxtField.setLeftView(image: UIImage.init(named: "user")!)
            userNameTxtField.tintColor = .darkGray
            userNameTxtField.autocorrectionType = .no
        }
     }
    @IBOutlet weak var LogoImage: UIImageView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var CreateBtn: UIButton!
    @IBOutlet weak var SignInBtn: UIButton!
    @IBOutlet var view1: UIView!
    @IBOutlet weak var CreateView: UIView!
    
    var iconClick = false
    var imageIcon = UIImageView()
    
    
///************************************************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.PasswordTextField.isSecureTextEntry = true
        
        ImageToggle()
        CreateBtn.layer.cornerRadius = 5
        SignInBtn.layer.cornerRadius = 5
        LogoImage.layer.cornerRadius = 10.0
        //CreateView.layer.cornerRadius = 20
        
        let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blurredView.frame = self.view.bounds
        blurredView.alpha = 0.5
        blurredView.backgroundColor = .black
        view.addSubview(blurredView)
        view.bringSubviewToFront(CreateView)
        CreateView.backgroundColor = .none
        
        
        
        
        
        //End of ViewDidLoad
    }
///************************************************************************
    //MARK: API Working
    var Usertype = "User"
    
    func PostDataToAPI(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/User/CreateAccount")!
        let para : [String:Any] = [
            "UName": self.userNameTxtField!,
            "UEmail" : self.EmailTextField!,
            "UPassword" : self.PasswordTextField!,
            "UType" : self.Usertype
        ]
        
        
        AF.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).validate().responseJSON{
            (response) in
            switch response.result{
            case .success(_):
                print(response)
            case .failure(_):
                print(Error.self)
            }
            
        }
        
        
    }
    
///************************************************************************
    //MARK: Buttons Action
    
    //SignIn Button Action
    @IBAction func SignInBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "SignInID") as! SignInController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //Create Button Action
    @IBAction func CreateBtn(_ sender: Any) {
        
        // --CODE HERE--
        
        let user = User()
        user.UName = userNameTxtField.text!
        let name: String? = user.UName
        user.UEmail = EmailTextField.text!
        let email: String? = user.UEmail
        user.UPassword = PasswordTextField.text!
        let password: String? = user.UPassword

        let encoder = JSONEncoder()
        let data = try! encoder.encode(user)
        let db = APIWrapper()
        let result = db.postMethodCall(controllerName: "User", actionName: "CreateAccount", httpBody: data)
        if result.ResponseCode == 200 {
            print("Done")
        }
        else{
                print(result.ResponseMessage)
        }
        if name != "" &&  email != "" && password != "" {
            let vc = storyboard?.instantiateViewController(identifier: "CustomTabBar") as! CustomUITabBarController
            self.navigationController?.pushViewController(vc, animated: true)
        }
            else{
            displayErrorMessage(message: "Please Fill All Textfields")
    }
        
        
        
        
        
  //      PostDataToAPI()
        
        
}
    
///************************************************************************
    //MARK: Fucntions
    
    func displayErrorMessage(message:String) {
         let alertView = UIAlertController(title: " ", message: message, preferredStyle: .alert)
         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
         }
         alertView.addAction(OKAction)
         if let presenter = alertView.popoverPresentationController {
             presenter.sourceView = self.view
             presenter.sourceRect = self.view.bounds
         }
         self.present(alertView, animated: true, completion:nil)
     }
    
    fileprivate func ImageToggle()  {
        // Toggle Image

        imageIcon.image = UIImage(named: "eyeslash")
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.frame = CGRect(x: -10, y: 0, width: 20, height: 20)

        imageIcon.frame = CGRect(x: -10, y: 0, width: 20, height: 20)

        PasswordTextField.rightView = contentView
        PasswordTextField.rightViewMode = .always

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)

    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){

        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if iconClick{

            iconClick = false
            tappedImage.image = UIImage(named: "eye")
            PasswordTextField.isSecureTextEntry = false
        }
        else{
            iconClick = true
            tappedImage.image = UIImage(named: "eyeslash")
            PasswordTextField.isSecureTextEntry = true
        }

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 8
    }



}
