//
//  UploadVideoViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 31/03/2021.
//

import UIKit
import AVKit

class UploadVideoViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    
    @IBOutlet weak var ChooseVideobtn: UIButton!
    
    
    @IBOutlet weak var UploadVideoBTN: UIButton!
    
    @IBOutlet weak var VideoTitleTextfield: UITextField!
    
    @IBOutlet weak var VideoCategoryTextField: UITextField!
    
    @IBOutlet weak var GenresVideoTextField: UITextField!
    
    @IBOutlet weak var VideoView: UIImage!
    
    @IBOutlet weak var UploadVideologo: UIImageView!
    var videopicker = UIImagePickerController()
    var videoURL: URL?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videopicker.delegate = self

        AVAsset(url: videoURL!).generateThumbnail { [weak self] (image) in
                       DispatchQueue.main.async {
                        guard let image = image else { return }
                        //self?.VideoView.image = image
                       }
                   }
    }
    
    @IBAction func ChooseVideoBtn(_ sender: Any) {
        
            videopicker.sourceType = .savedPhotosAlbum
            videopicker.delegate = self
            present(videopicker, animated: true, completion: nil)
        

    }
    
   
    @IBAction func UploadVideoBtn(_ sender: Any) {
        
        
    }
    

    
   
    
    
     
    
}

extension AVAsset {

    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 0.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
