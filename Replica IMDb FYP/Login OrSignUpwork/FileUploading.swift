//
//  FileUploading.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 02/04/2021.
//

import Foundation

class Videos:Codable{
    var ImageID : Int = 0
    var VID : Int = 0
    var ImageName : String = ""
    var ImagePath : String = ""
}
