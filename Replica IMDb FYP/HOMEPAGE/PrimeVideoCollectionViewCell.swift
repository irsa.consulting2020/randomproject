//
//  PrimeVideoCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 26/03/2021.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON

class PrimeVideoCollectionViewCell: UICollectionViewCell {
    
    var fadingLabel: UILabel!
    @IBOutlet weak var PrimeVideoImageView: UIImageView!
    @IBOutlet weak var PrimeVideoCosmoView: CosmosView!
    @IBOutlet weak var PrimeVideoNameLbl: UILabel!
    @IBOutlet weak var PrimeVideoWatchlistBtn: UIButton!
    var delegate: PrimeCollectionDelegate?

    @IBOutlet weak var Addbtn: UIButton!
    @IBOutlet weak var AddTag: UIImageView!
    var movieId = 0
    var index = 0
    
    override func awakeFromNib() {
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PrimeVideoCollectionViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
        
        ///***********************************************************
                // Fading Label Working
        fadingLabel = UILabel()
        fadingLabel.text = "Text"
        fadingLabel.textColor = .black
        contentView.addSubview(fadingLabel)
        fadingLabel.isHidden = true
        fadingLabel.translatesAutoresizingMaskIntoConstraints = false
        fadingLabel.topAnchor.constraint    (equalTo: contentView.topAnchor,   constant:     150).isActive = true
        fadingLabel.leftAnchor.constraint   (equalTo: contentView.leftAnchor,  constant:     3).isActive = true
        fadingLabel.widthAnchor.constraint  (equalTo: contentView.widthAnchor, multiplier:  0.95).isActive = true
        fadingLabel.heightAnchor.constraint (equalToConstant: 30).isActive = true
        
        ///***********************************************************
            //Cell Designing
        self.contentView.layer.cornerRadius = 7.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        PrimeVideoImageView.layer.cornerRadius = 7
        
        
       //End of ViewDidLoad
    }
    ///************************************************************
//MARK: API Working
    
    var watch = [Watchlist]()
    
    func InsertDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)&rId=\(RoleId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        ]
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
                self.delegate?.fetchWatchlistData()
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            self.delegate?.fetchWatchlistData()
        }
    }
    
    
    
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.PrimeCollectionDelegate(mIndex: index)
        }
    @objc private func didTapWatchlist(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        }
    @objc private func btnAction(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.PrimeBtnAction()
        }
    ///***********************************************************
            //MARK: Watchlist Working
    
    @IBAction func AddWatchlist(_ sender: Any) {
        print("Tapped")
       
        if usrlogin != nil{
        if PrimeVideoWatchlistBtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            Addbtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            Addbtn.tintColor = .black
            Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            PrimeVideoWatchlistBtn.setTitle("✔️ Watchlisted", for: .normal)
            PrimeVideoWatchlistBtn.setTitleColor(.black, for: .normal)
            PrimeVideoWatchlistBtn.tag = 2
            print(PrimeVideoWatchlistBtn.tag)
            fadeMessage(message: "Added to Watchlist!", color: .black, finalAlpha: 0.0)
            InsertDataInApi()
        }else{
            if PrimeVideoWatchlistBtn.tag == 2 {
               
                AddTag.tintColor = .black
                Addbtn.setImage(UIImage(systemName: "plus"), for: .normal)
                Addbtn.tintColor = .white
                Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
                PrimeVideoWatchlistBtn.setTitle("+ Watchlist", for: .normal)
                PrimeVideoWatchlistBtn.setTitleColor(.systemBlue, for: .normal)
                PrimeVideoWatchlistBtn.tag = 0
                print(PrimeVideoWatchlistBtn.tag)
                fadeMessage(message: "Removed to Watchlist!", color: .black, finalAlpha: 0.0)
                DeleteWatchlistData()
            }
          }
        }else{
            Addbtn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        }
    }
    @IBAction func watclist(_ sender: Any) {
        
        print("Tapped")
       
        if usrlogin != nil {
        if PrimeVideoWatchlistBtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            Addbtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            Addbtn.tintColor = .black
            Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            PrimeVideoWatchlistBtn.setTitle("✔️ Watchlisted", for: .normal)
            PrimeVideoWatchlistBtn.setTitleColor(.black, for: .normal)
            PrimeVideoWatchlistBtn.tag = 2
            print(PrimeVideoWatchlistBtn.tag)
            fadeMessage(message: "Added to Watchlist!", color: .black, finalAlpha: 0.0)
            InsertDataInApi()
        }else{
            if PrimeVideoWatchlistBtn.tag == 2 {
               
                AddTag.tintColor = .black
                Addbtn.setImage(UIImage(systemName: "plus"), for: .normal)
                Addbtn.tintColor = .white
                Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
                PrimeVideoWatchlistBtn.setTitle("+ Watchlist", for: .normal)
                PrimeVideoWatchlistBtn.setTitleColor(.systemBlue, for: .normal)
                PrimeVideoWatchlistBtn.tag = 0
                print(PrimeVideoWatchlistBtn.tag)
                fadeMessage(message: "Removed to Watchlist!", color: .black, finalAlpha: 0.0)
                DeleteWatchlistData()
            }
          }
        }
        else{
            Addbtn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        }
    }
    
    
    ///***********************************************************
            //MARK: Fading Label
        func fadeMessage(message: String, color: UIColor, finalAlpha: CGFloat) {
            fadingLabel.text          = message
            fadingLabel.textColor = .black
            fadingLabel.alpha         = 1.0
            fadingLabel.isHidden      = false
            fadingLabel.textAlignment = .center
            fadingLabel.backgroundColor     = .white
            fadingLabel.layer.cornerRadius  = 5
            fadingLabel.layer.masksToBounds = true
            fadingLabel.font = .boldSystemFont(ofSize: 12)
            UIView.animate(withDuration: 3.0, animations: { () -> Void in
                self.fadingLabel.alpha = finalAlpha
            })
        }
        
        
    //End of class
}
///***********************************************************

protocol PrimeCollectionDelegate {
    func PrimeCollectionDelegate(mIndex: Int)
    func PrimeBtnAction()
    func fetchWatchlistData()
}
