//
//  HomeTabBarController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 20/03/2021.
//

import UIKit
import Cosmos
import DropDown
import Alamofire
import SwiftyJSON

var seeAll = false
var RoleId = 0
var WatchId = 0
var movieIndex : [Int]? = nil
var homeTab : HomeTabBarController? = nil

class HomeTabBarController: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource,UICollectionViewDelegate, FeatureCollectionDelegate, PrimeCollectionDelegate, CoomingCollectionDelegate, IMDbCollectionDelegate, FanFavCollectionDelegate, SlidesCollectionDelegate, watchlist,Refresh, UIPopoverPresentationControllerDelegate{
    func choseWatchlistData() {
        let vc = storyboard?.instantiateViewController(identifier: "ChoseRole") as! ChoseRoleViewController
            // self.navigationController?.pushViewController(vc, animated: true)
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.delegate = self
        vc.popoverPresentationController?.sourceRect = .zero
        self.navigationController?.present(vc, animated: true)
    }
    
    
    
    func slideBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: ProtocolDelegation
    func PrimeBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func CoomingBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func IMDbBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func FanFavBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func featuredBtnAction() {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: CollectionView Delegation
    
    func FeatureCollectionDelegate(mIndex:Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = movie[mIndex]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    func FanFavCollectionDelegate(mIndex: Int) {
        print("Tapped")
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = movie[mIndex]
        if isSearching == true {
            vc.MovieDetail = GenresDropDown[mIndex]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func IMDbCollectionDelegate(mIndex:Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = movie[mIndex]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    func PrimeCollectionDelegate(mIndex:Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = movie[mIndex]
        if isSearching == true {
            vc.MovieDetail = GenresDropDown2[mIndex]
        }
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    func CoomingCollectionDelegate(mIndex:Int) {
        let vc = storyboard?.instantiateViewController(identifier: "ComingSoonVideoPlayer") as! ComingSoonVideoPlayingViewController
        vc.MovieDetail = movie[mIndex]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
///************************************************************************
    //MARK: Initializers
    internal init(slides: [slides] = [], scrollView: UIScrollView? = nil, paging: UIPageControl? = nil, timer: Timer = Timer()) {
        self.slides = slides
        self.scrollView = scrollView
        self.paging = paging
        self.timer = timer
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)    }
    
///************************************************************************
    //MARK: Variable Declaration for Slide
    
    // variables declaration for first view
    
    var pages : [slides]{
        get{
            
            let slide1:slides = Bundle.main.loadNibNamed("slidesHomeTabBar", owner: self, options: nil)?.first as! slides
            slide1.imageView.image = UIImage(named: "Marriage&Mortage")
            slide1.videosTitleLabel.text = "Marriage or Mortgage"
            slide1.videosView.backgroundColor = UIColor.blue
            slide1.Imageview1.image = UIImage(named: "marriageandmortage")
            slide1.delegate = self
            
            let slide2:slides = Bundle.main.loadNibNamed("slidesHomeTabBar", owner: self, options: nil)?.first as! slides
            slide2.imageView.image = UIImage(named: "ParadisePD")
            slide2.videosTitleLabel.text = "Paradise P.D"
            slide2.videosView.backgroundColor = UIColor.red
            slide2.Imageview1.image = UIImage(named: "paradisepd2")
            slide2.delegate = self
        
            let slide3:slides = Bundle.main.loadNibNamed("slidesHomeTabBar", owner: self, options: nil)?.first as! slides
            slide3.imageView.image = UIImage(named: "ZeroChill")
            slide3.videosTitleLabel.text = "Zero Chill"
            slide3.videosView.backgroundColor = UIColor.brown
            slide3.Imageview1.image = UIImage(named: "zerochill2")
            slide3.delegate = self
        
            let slide4:slides = Bundle.main.loadNibNamed("slidesHomeTabBar", owner: self, options: nil)?.first as! slides
            slide4.imageView.image = UIImage(named: "Waffles+Mochi")
            slide4.videosTitleLabel.text = "Waffles + Mochi"
            slide4.videosView.backgroundColor = UIColor.purple
            slide4.Imageview1.image = UIImage(named: "wafflesandmochi2")
            slide4.delegate = self
            
            let slide5:slides = Bundle.main.loadNibNamed("slidesHomeTabBar", owner: self, options: nil)?.first as! slides
            slide5.imageView.image = UIImage(named: "LastChanceUBasketBall")
            slide5.videosTitleLabel.text = "Last Chance U: Basketball"
            slide5.videosView.backgroundColor = UIColor.orange
            slide5.Imageview1.image = UIImage(named: "LastChancebasketball2")
            slide5.delegate = self
            return [slide1, slide2, slide3, slide4, slide5]
        }
    }
    
    var slides:[slides] = [];
    var timer: Timer = Timer()
    
    var dropDown = DropDown()
    var typeDropDown = DropDown()
    var isSearching = false
    
    @IBOutlet weak var beforeLoginWatchlist: UIView!
    @IBOutlet weak var AfterLoginWatchlist: UIView!
    @IBOutlet weak var WatchlistView: UIView!
    @IBOutlet weak var WatchlistCollectionView: UICollectionView!
    
    
    
///************************************************************************
    //MARK: Outlets
    
    // Outlets for First View
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var paging: UIPageControl!
    
    // OUTLETS FOR COLLECTION VIEW
    
    @IBOutlet weak var FeaturedCollectionView: UICollectionView!
    @IBOutlet weak var FanFavCollectionView: UICollectionView!
    @IBOutlet weak var IMDbsOriginalCollectionView: UICollectionView!
    @IBOutlet weak var PrimeVideosCollectionView: UICollectionView!
    @IBOutlet weak var ComingSoonCollectionView: UICollectionView!
    
    // OUTLETS FOR Buttons and labels
    
    @IBOutlet weak var WatchlistLabel: UILabel!
    @IBOutlet weak var Fanfavlbl: UILabel!
    @IBOutlet weak var IMDbsOriginalLbl: UILabel!
    @IBOutlet weak var fanfavTypebtn: UIButton!
    @IBOutlet weak var favfavGenresBtn: UIButton!
    @IBOutlet weak var PrimeVideolbl: UILabel!
    @IBOutlet weak var PrimeVideoTypeBtn: UIButton!
    @IBOutlet weak var PrimeVideoGenresBtn: UIButton!
    @IBOutlet weak var ComingSoonLbl: UILabel!
    @IBOutlet weak var TopNewsLabl: UILabel!
    @IBOutlet weak var FollowImdbLbl: UILabel!
    @IBOutlet weak var FacebookBtn: UIButton!
    @IBOutlet weak var InstagramBtn: UIButton!
    @IBOutlet weak var TwitterBtn: UIButton!
    @IBOutlet weak var YoutubeBtn: UIButton!
    
///************************************************************************
    //MARK: ViewDidLoad
    
    override func viewWillAppear(_ animated: Bool) {
        fetchWatchlistData()
        fetchRoleApi()
        fetchNewWatchlistData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        homeTab = self 
//        let vc1 = FanFavouriteViewController()
//        vc1.delegate1 = self
//        vc1.present(vc1, animated: true)
        
        //MARK: Designing
        
        WatchlistLabel.layer.cornerRadius = 4
        Fanfavlbl.layer.cornerRadius = 4
        IMDbsOriginalLbl.layer.cornerRadius = 4
        fanfavTypebtn.layer.cornerRadius = 20
        favfavGenresBtn.layer.cornerRadius = 20
        PrimeVideolbl.layer.cornerRadius = 4
        PrimeVideoTypeBtn.layer.cornerRadius = 20
        PrimeVideoGenresBtn.layer.cornerRadius = 20
        ComingSoonLbl.layer.cornerRadius = 4
//        TopNewsLabl.layer.cornerRadius = 4
        FollowImdbLbl.layer.cornerRadius = 4
        WatchlistLabel.clipsToBounds = true
        Fanfavlbl.clipsToBounds = true
        IMDbsOriginalLbl.clipsToBounds = true
        PrimeVideolbl.clipsToBounds = true
        ComingSoonLbl.clipsToBounds = true
   //     TopNewsLabl.clipsToBounds = true
        FollowImdbLbl.clipsToBounds = true

        FacebookBtn.layer.cornerRadius = 10
        InstagramBtn.layer.cornerRadius = 10
        TwitterBtn.layer.cornerRadius = 10
        YoutubeBtn.layer.cornerRadius = 10
        FacebookBtn.clipsToBounds = true
        InstagramBtn.clipsToBounds = true
        YoutubeBtn.clipsToBounds = true
        TwitterBtn.clipsToBounds = true
        
        ///*****************************
        
        //MARK: Timer for slider
        // Working of Auto Movement of videos for the first view
        
        scrollView.delegate = self
        
        setupSlideScrollView(pages: pages)
        paging.numberOfPages = pages.count
        paging.currentPage = 0
        paging.isHidden = true
         
    
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.animateScrollView), userInfo: nil, repeats: true)
     //  timer.invalidate()
        
        
        ///********************************
        //MARK: Collection Delegation
      
        FeaturedCollectionView.delegate = self
        FeaturedCollectionView.dataSource = self

        FanFavCollectionView.delegate = self
        FanFavCollectionView.dataSource = self

        IMDbsOriginalCollectionView.delegate = self
        IMDbsOriginalCollectionView.dataSource = self

        PrimeVideosCollectionView.delegate = self
        PrimeVideosCollectionView.dataSource = self

        ComingSoonCollectionView.delegate = self
        ComingSoonCollectionView.dataSource = self
        
        WatchlistCollectionView.delegate = self
        WatchlistCollectionView.dataSource = self
        
        ///*************************************
        
        //MARK: DropDown Appearence
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
     //   DropDown.appearance().selectedTextColor = UIColor.systemBlue
        
        ///**************************************
        
        fetchCollectionViewApiData()
            fetchGenresapi()
            fetchTypeapi()
        fetchNewWatchlistApi()
        fetchNewWatchlistData()
        
       // fetchWatchlistData()
        fetchFanFavApiData()
        fetchGenresApiData(text: "All", text2: "All")
        fetchGenresApiData2(text: "All", text2: "All")
        fetchRoleApi()
        SortedByDateNewWatchlistData()
        SortedByRatingNewWatchlistData()
        
//        fetchNewWatchlistApi()
//        fetchNewWatchlistData()
      //End Of viewdidLoad()
    }
    
///************************************************************************
    //MARK: API Working
    
    // ACTIONS FOR TYPE AND GENRES BUTTONS
   
    var category1 = [Genres]()
    var type = [Type]()
    var list: [String] = []
    func fetchGenresapi(){
        let url = "http://10.211.55.3/replicaimdbapi/api/Genres/AllGenres"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
               // print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                for i in myresult! {
                    let id = i["Genres_Id"].intValue
                    let genres = i["Genres_Name"].stringValue

                    self.category1.append(Genres(Genres_Id: id, Genres_Name: genres))
                    self.list.append(genres)
                    //print(self.category1)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    var categoryList : [String] = []
    func fetchTypeapi(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/type/alltypes"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
           //     print(myresponse)
           //     print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
          //      print(myresult!)
                for i in myresult! {
                    let id = i["Type_Id"].intValue
                    let type = i["Type_Name"].stringValue

                    self.type.append(Type(Type_Id: id, Type_Name: type))
                    self.categoryList.append(type)
                   // print(self.category1)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }

    
    
    var movie = [Movie]()
    var Coming = [Movie]()
    var IMDb = [Movie]()
    var Prime = [Movie]()
    func fetchCollectionViewApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AllMovies"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)
                

                self.movie.append(movierecord)
                if Movie_Status == "Coming" {
                    self.Coming.append(movierecord)
                }else{
                    if Movie_Status == "IMDb" {
                        self.IMDb.append(movierecord)
                    }else{
                        if Movie_Status == "Prime" {
                            self.Prime.append(movierecord)
                        }
                    }
                }
            }
            self.FeaturedCollectionView.reloadData()
            self.FanFavCollectionView.reloadData()
            self.ComingSoonCollectionView.reloadData()
            self.PrimeVideosCollectionView.reloadData()
            self.IMDbsOriginalCollectionView.reloadData()
            self.WatchlistCollectionView.reloadData()
        }
    }
    var Watchlist = [Movie]()
    func fetchWatchlistData(){
       var id = 0
        Watchlist.removeAll()
        if usrlogin != nil {
            id = usrlogin!.UId
        }
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/watchlist?id=\(String(describing: id))&rid=\(rId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
            let response = response.data!
            let array = JSON(response).arrayValue
            self.Watchlist.removeAll()
            for record in array{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.Watchlist.append(movierecord)
            }
            self.watchlist()
            self.WatchlistCollectionView.reloadData()
        }
    }
    var GenresDropDown = [Movie]()
    var GenresDropDown2 = [Movie]()
    func fetchGenresApiData(text: String, text2: String){
        GenresDropDown.removeAll()
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Genres/GetDropDownSearch?lookfor=\(text)&type=\(text2)"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.GenresDropDown.append(movierecord)
               // self.GenresDropDown2.append(movierecord)
            }
            self.FanFavCollectionView.reloadData()
            //self.PrimeVideosCollectionView.reloadData()
        }
    }
    func fetchGenresApiData2(text: String, text2: String){
        GenresDropDown2.removeAll()
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Genres/GetDropDownSearch?lookfor=\(text)&type=\(text2)"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                //self.GenresDropDown.append(movierecord)
                self.GenresDropDown2.append(movierecord)
            }
            //self.FanFavCollectionView.reloadData()
            self.PrimeVideosCollectionView.reloadData()
        }
    }
    var fanFavMovie = [Movie]()
    func fetchFanFavApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/FanFavourite"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
        //    print(response)
            let JsonArray = JSON(response).arrayValue
        //    print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)
                

                self.fanFavMovie.append(movierecord)
            }
            self.FanFavCollectionView.reloadData()
        }
    }
///************************************************************************
    //MARK: DropDown Actions

    @IBAction func FanFavTypeBtn(_ sender: Any) {
        
        dropDown.dataSource = categoryList
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
                self!.fetchGenresApiData(text: (self!.favfavGenresBtn.titleLabel?.text)!, text2: (self!.fanfavTypebtn.titleLabel?.text)!)
                self!.isSearching = true
            }
       
    }
    @IBAction func FanfavGenresBtn(_ sender: Any) {
        
        dropDown.dataSource = list //["All","Action","Adventure","Animation","Comedy","Crime","Drama","Fantasy","Horror","Thriller","Western"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
                self!.fetchGenresApiData(text: (self!.favfavGenresBtn.titleLabel?.text)!, text2: (self!.fanfavTypebtn.titleLabel?.text)!)
                self!.isSearching = true
            }
        
    }
    @IBAction func PrimeVideosTypeBtn(_ sender: Any) {
        
        dropDown.dataSource = categoryList
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
                self!.fetchGenresApiData2(text: (self!.PrimeVideoGenresBtn.titleLabel?.text)!, text2: (self!.PrimeVideoTypeBtn.titleLabel?.text)!)
                self!.isSearching = true
            }
        
    }
    @IBAction func PrimeVideosGenresBtn(_ sender: Any) {
        
        
        dropDown.dataSource = list //["All","Action","Adventure","Animation","Comedy","Crime","Drama","Fantasy","Horror","Thriller","Western"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
                self!.fetchGenresApiData2(text: (self!.PrimeVideoGenresBtn.titleLabel?.text)!, text2: (self!.PrimeVideoTypeBtn.titleLabel?.text)!)
                self!.isSearching = true
            }
        
    }
    
///************************************************************************
    //MARK: ScrollView Slider
    
// Functions and Obj for Auto movement working of videos for first view
    
  @objc  func animateScrollView() {
            let scrollWidth = scrollView.bounds.width
            let currentXOffset = scrollView.contentOffset.x

            let lastXPos = currentXOffset + scrollWidth
            if lastXPos != scrollView.contentSize.width {
               // print("Scroll")
                scrollView.setContentOffset(CGPoint(x: lastXPos, y: 0), animated: true)
            }
            else {
               // print("Scroll to start")
                scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            }

  }
   
   
    func setupSlideScrollView(pages : [slides]) {
           scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 300)
           scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(pages.count), height: 300)
           scrollView.isPagingEnabled = true
           
           for i in 0 ..< pages.count {
               pages[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: 300)
               scrollView.addSubview(pages[i])
           }
       }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        paging.currentPage = Int(pageIndex)
    
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
               let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
               let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
               
               let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
               let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
               
               if(percentOffset.x > 0 && percentOffset.x <= 0.25) {
                   
                   pages[0].imageView.transform = CGAffineTransform(scaleX: (0.25-percentOffset.x)/0.25, y: (0.25-percentOffset.x)/0.25)
                pages[1].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.25, y: percentOffset.x/0.25)
                   
               } else if(percentOffset.x > 0.25 && percentOffset.x <= 0.50) {
                pages[1].imageView.transform = CGAffineTransform(scaleX: (0.50-percentOffset.x)/0.25, y: (0.50-percentOffset.x)/0.25)
                pages[2].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.50, y: percentOffset.x/0.50)
                   
               } else if(percentOffset.x > 0.50 && percentOffset.x <= 0.75) {
                pages[2].imageView.transform = CGAffineTransform(scaleX: (0.75-percentOffset.x)/0.25, y: (0.75-percentOffset.x)/0.25)
                pages[3].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.75, y: percentOffset.x/0.75)
                   
               } else if(percentOffset.x > 0.75 && percentOffset.x <= 1) {
                pages[3].imageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
                pages[4].imageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
               }
    }
    
    
    
///************************************************************************
    //MARK: Collection Views
            // WORKING FOR COLLECTION VIEWS

    var items = ["Fate","DragMeToHell","TheWitcher","Sabrina","theWorstWitch"]
    var videoaTitle = ["FateThe winx Saga","Drag me to hell","The Witcher","Chilling Adventures of Sabrina","The Worst Witch"]

    var favitems = ["Fate","DragMeToHell","TheWitcher","Sabrina","theWorstWitch"]
    var favvideoaTitle = ["FateThe winx Saga","Drag me to hell","The Witcher","Chilling Adventures of Sabrina","The Worst Witch"]
    var favcosmo = ["8.1","7.6","8.5","9.2","6.5"]

    
    var sortdate = false
    var sortRating = false
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.FeaturedCollectionView:
            return self.movie.count
        case self.FanFavCollectionView:
            if isSearching == true {
                return self.GenresDropDown.count
            }else{
                return self.fanFavMovie.count
            }
             //   return self.fanFavMovie.count
        case self.IMDbsOriginalCollectionView:
            return self.IMDb.count
        case self.PrimeVideosCollectionView:
            if isSearching == true {
                return self.GenresDropDown2.count
            }else{
                return self.Prime.count
            }
           // return self.GenresDropDown2.count
        case self.ComingSoonCollectionView:
            return self.Coming.count
        case self.WatchlistCollectionView:
            if sortdate == true{
                return self.sortedByDate.count
            }else{
                if sortRating == true {
                    return self.sortedByRating.count
                }
            }
            return self.Newdata.count
        default:
            return 0
        }
        
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.WatchlistCollectionView:
            let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
            vc.MovieDetail = Watchlist[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
            print("Tapped")
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        switch collectionView {
        case self.FeaturedCollectionView:
            let cell: FeaturedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCell", for: indexPath as IndexPath) as! FeaturedCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//         cell.FeaturedImageView.image = img
       //  cell.FeaturedVideosNameLbl.text = videoaTitle[indexPath.row]
            
            cell.delegate = self
            let featured = movie[indexPath.row]
            cell.FeaturedVideosNameLbl.text = featured.Movie_Name
            cell.index = indexPath.row
            cell.movieId = featured.Movie_Id
            // cell.rid = roleId[<#Int#>]
            AF.request( featured.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell.FeaturedImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
           return cell
        case self.FanFavCollectionView:
            
            let cell2: FanFavCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FanFavCell", for: indexPath as IndexPath) as! FanFavCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell2.FanFavImageView.image = img
//            cell2.FanFavVideosNamelbl.text = videoaTitle[indexPath.row]
//            cell2.FanFavCosmosView.text = favcosmo[indexPath.row]
            
            cell2.delegate = self
            cell2.index = indexPath.row
            var featured = Movie()
            if isSearching == true {
                featured = GenresDropDown[indexPath.row]
            }else{
                if isSearching == false {
                    featured = fanFavMovie[indexPath.row]
                }
            }
            cell2.FanFavVideosNamelbl.text = featured.Movie_Name
            cell2.FanFavCosmosView.text = "\(featured.Movie_Rating)"
            cell2.movieId = featured.Movie_Id
            AF.request( featured.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell2.FanFavImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
            
                        
           return cell2
        case self.IMDbsOriginalCollectionView:
           
            let cell3: IMDbOriginalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "IMDbsOriginal", for: indexPath as IndexPath) as! IMDbOriginalCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell3.IMDbsOriginalImageView.image = img
//            cell3.IMDbsOriginalVideoNamelbl.text = videoaTitle[indexPath.row]
            
            cell3.delegate = self
            cell3.index = indexPath.row

            let featured = IMDb[indexPath.row]
            cell3.IMDbsOriginalVideoNamelbl.text = featured.Movie_Name
            cell3.IMDbsOriginalTimeLbl.text = featured.Movie_Time
            cell3.movieId = featured.Movie_Id
            AF.request( featured.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell3.IMDbsOriginalImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
                return cell3
            
        case self.PrimeVideosCollectionView:
           
            let cell4: PrimeVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PrimeVideos", for: indexPath as IndexPath) as! PrimeVideoCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell4.PrimeVideoImageView.image = img
//            cell4.PrimeVideoNameLbl.text = videoaTitle[indexPath.row]
//            cell4.PrimeVideoCosmoView.text = favcosmo[indexPath.row]
            
            cell4.delegate = self
            cell4.index = indexPath.row
            var featured = Movie()
            if isSearching == true {
                featured = GenresDropDown2[indexPath.row]
            }else{
                if isSearching == false {
                    featured = Prime[indexPath.row]
                }
            }
            cell4.PrimeVideoNameLbl.text = featured.Movie_Name
            cell4.PrimeVideoCosmoView.text = "\(featured.Movie_Rating)"
            cell4.movieId = featured.Movie_Id
            AF.request( featured.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell4.PrimeVideoImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
                return cell4
        case self.ComingSoonCollectionView:
           
            let cell4: ComingSoonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComingSoon", for: indexPath as IndexPath) as! ComingSoonCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell4.ComingSoonImageView.image = img
//            cell4.ComingSoonVideoNameLbl.text = videoaTitle[indexPath.row]
            
            cell4.delegate = self
            cell4.index = indexPath.row
            let featured = Coming[indexPath.row]
            cell4.ComingSoonVideoNameLbl.text = featured.Movie_Name
            cell4.movieId = featured.Movie_Id
            AF.request( featured.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell4.ComingSoonImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
                return cell4
        case self.WatchlistCollectionView:
           
            let cell4: Watchlist2CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "watchlist", for: indexPath as IndexPath) as! Watchlist2CollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell4.ComingSoonImageView.image = img
//            cell4.ComingSoonVideoNameLbl.text = videoaTitle[indexPath.row]
            
            cell4.delegate = self
            var featured = Movie()
            if sortdate == true {
                featured = sortedByDate[indexPath.row]
            }else{
                if sortRating == true {
                    featured = sortedByRating[indexPath.row]
                }else{
                    featured = self.Newdata[indexPath.row]
                }
            }
                
            cell4.WatchlistVideoNameLbl.text = featured.Movie_Name
            cell4.WatchlistRating.text = "\(featured.Movie_Rating)"
            cell4.movieId = featured.Movie_Id
            AF.request( featured.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell4.WatchlistImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
            
                return cell4
        default:
            return UICollectionViewCell()
    }
    
}
///************************************************************************
  //MARK: Watchlist Sign-In
    
    
    @IBAction func WatchlistSignInBtn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "SecondScreenSignIn") as! SecondSignInScreenController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func BrowseBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        vc.movieDetail = movie
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func SeeAlWatchlist(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "WatchlistSeeAll") as! WatchlistSeeAllViewController
        vc.movieDetail = Watchlist
        seeAll = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
///************************************************************************
    //MARK: See All Buttons
    
    @IBAction func FanfavSeeAllBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        vc.movieDetail = movie
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func ComingSoonSeeALLbtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        vc.movieDetail = movie
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func TopNewsSeeAllBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
///************************************************************************
    
    //MARK: Watchist Work
    
    func watchlist(){
        
        if usrlogin != nil {
            print("User Logged In")
            
            let c = Newdata.count//.compactMap({ $0 }).count + 1
            //print("count", c)
            if c == 0 {
                beforeLoginWatchlist.isHidden = true
                AfterLoginWatchlist.isHidden = false
                WatchlistView.isHidden = true
                selfButton.isHidden = false
                AddPersonBtn.isHidden = false
            }else{
                if c != 0 {
                    beforeLoginWatchlist.isHidden = true
                    AfterLoginWatchlist.isHidden = true
                    WatchlistView.isHidden = false
                    selfButton.isHidden = false
                    AddPersonBtn.isHidden = false
                }
            }
        }else
        {
            print("Not Logged in")
            beforeLoginWatchlist.isHidden = false
            AfterLoginWatchlist.isHidden = true
            WatchlistView.isHidden = true
            selfButton.isHidden = true
            AddPersonBtn.isHidden = true
        }
    }
    
    
   //MARK: Multiple Watchlist Working
    
    @IBOutlet weak var selfButton: UIButton!
    @IBOutlet weak var AddPersonBtn: UIButton!
    
    
    @IBAction func SelfButton(_ sender: Any) {
        dropDown.dataSource = Rolelist
                      dropDown.anchorView = (sender as! AnchorView)
                      dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
                          dropDown.show()
                          dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                              (sender as AnyObject).setTitle(item, for: .normal)
                              self!.rId = self!.role[index].Role_Id
                            RoleId = self!.rId 
                              self!.fetchWatchlistData()
                              
                          }
                }
            
            
            
    var rId = 0
    @IBAction func AddBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "AddPerson") as! AddPersonViewController
           
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.delegate = self
        vc.popoverPresentationController?.sourceRect = .zero
        self.navigationController?.present(vc, animated: true)
    }
    
    
    var role = [Role]()
    var Rolelist: [String] = []
    var roleId: [Int] = []
    
    func fetchRoleApi(){
        var id = 0
        // movie.removeAll()
         if usrlogin != nil {
             id = usrlogin!.UId
         }
        let url = "http://10.211.55.3/replicaimdbapi/api/Movie/RoleList?id=\(id)"
        AF.request(url, method: .get).responseJSON { [self]
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
               // print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                
                role.removeAll()
                Rolelist.removeAll()
                for i in myresult! {
                    let id = i["Role_Id"].intValue
                    let roleName = i["Role_Name"].stringValue

                    self.role.append(Role(Role_Id: id, Role_Name: roleName))
                    self.Rolelist.append(roleName)
                    self.roleId.append(id)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    
    
   //MARK: Working For Task
    
    @IBAction func RatingSorted(_ sender: Any) {
        sortRating = true
        SortedByRatingNewWatchlistData()
    }
    @IBAction func DateSorted(_ sender: Any) {
        sortdate = true
        SortedByDateNewWatchlistData()
    }
    @IBAction func WatchlistName(_ sender: Any) {
        dropDown.dataSource = newlist
                      dropDown.anchorView = (sender as! AnchorView)
                      dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
                          dropDown.show()
                          dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                              (sender as AnyObject).setTitle(item, for: .normal)
                            self?.sortdate = false
                            self?.sortRating = false
                            self!.wId = self!.newWatch[index].W_Id
                            WatchId = self!.wId
                              self!.fetchNewWatchlistData()
                          }
    }
    var wId = 0
    
    @IBAction func AddNewWatchlist(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "NewWatchlist") as! AddNewWatchlistViewController
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.delegate = self
        vc.popoverPresentationController?.sourceRect = .zero
        self.navigationController?.present(vc, animated: true)
    }
    
    
    var newWatch = [NewWatchlist]()
    var newlist: [String] = []
    var newWatchId: [Int] = []
    
    func fetchNewWatchlistApi(){
        var id = 0
        // movie.removeAll()
         if usrlogin != nil {
             id = usrlogin!.UId
         }
        let url = "http://10.211.55.3/replicaimdbapi/api/Movie/NewWatchList?id=\(id)"
        AF.request(url, method: .get).responseJSON { [self]
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
               // print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                
                newWatch.removeAll()
                newlist.removeAll()
                for i in myresult! {
                    let id = i["W_Id"].intValue
                    let roleName = i["W_Name"].stringValue

                    self.newWatch.append(NewWatchlist(W_Id: id, W_Name: roleName))
                    self.newlist.append(roleName)
                    self.newWatchId.append(id)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    
    
    var Newdata = [Movie]()
    func fetchNewWatchlistData(){
       var id = 0
        Newdata.removeAll()
        if usrlogin != nil {
            id = usrlogin!.UId
        }
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/GetDropDown?gid=\(GenreId)&id=\(wId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
            let response = response.data!
            let array = JSON(response).arrayValue
            self.Newdata.removeAll()
            for record in array{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.Newdata.append(movierecord)
            }
            self.watchlist()
            self.WatchlistCollectionView.reloadData()
        }
    }
    
    var sortedByRating = [Movie]()
    func SortedByRatingNewWatchlistData(){
        sortedByRating.removeAll()
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/SortedByRating?wid=\(WatchId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
            let response = response.data!
            let array = JSON(response).arrayValue
            self.sortedByRating.removeAll()
            for record in array{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.sortedByRating.append(movierecord)
            }
            self.watchlist()
            self.WatchlistCollectionView.reloadData()
        }
    }
    
    var sortedByDate = [Movie]()
    func SortedByDateNewWatchlistData(){
        sortedByDate.removeAll()
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/SortedByDate?wid=\(WatchId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
            let response = response.data!
            let array = JSON(response).arrayValue
            self.sortedByDate.removeAll()
            for record in array{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.sortedByDate.append(movierecord)
            }
            self.watchlist()
            self.WatchlistCollectionView.reloadData()
        }
    }
    
    
    
    
    //End Of Class
}

protocol Refresh {
    func fetchWatchlistData()
}
