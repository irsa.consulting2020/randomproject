//
//  ChoseRoleViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 09/07/2021.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON

class ChoseRoleViewController: UIViewController {

    var Source : FeaturedCollectionViewCell? = nil
    
    @IBOutlet weak var okBtn: UIButton!
    
    @IBOutlet weak var ChoseView: UIView!
    
    @IBOutlet weak var RoleBtn: UIButton!
    var dropDown = DropDown()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChoseView.layer.cornerRadius = 7.0
        RoleBtn.layer.cornerRadius = 5.0
        okBtn.layer.cornerRadius = 5
        
       // fetchRoleApi()
        fetchNewWatchlistApi()
        
        //End Of ViewDidLoad
    }
   
    @IBAction func DoneBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func RoleBtn(_ sender: Any) {
        
        dropDown.dataSource = newlist
                      dropDown.anchorView = (sender as! AnchorView)
                      dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
                          dropDown.show()
                          dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                              (sender as AnyObject).setTitle(item, for: .normal)
                            NotificationCenter.default.post(name: Notification.Name("WatchlistForLogin"), object: nil)
                           
                            WatchId = (self?.newWatch[index].W_Id)!
                            if self?.Source != nil {
                                self!.Source?.InsertDataInApi1()
                            }
                           //  = vc.rId
 //                           vc.fetchWatchlistData()
//                              self!.rId = self!.role[index].Role_Id
//                            RoleId = self!.rId
//                              self!.fetchWatchlistData()
                              
                          }
        
        
    }
    var role = [Role]()
    var Rolelist: [String] = []
    var roleId: [Int] = []
    
    func fetchRoleApi(){
        var id = 0
        // movie.removeAll()
         if usrlogin != nil {
             id = usrlogin!.UId
         }
        let url = "http://10.211.55.3/replicaimdbapi/api/Movie/RoleList?id=\(id)"
        AF.request(url, method: .get).responseJSON { [self]
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
               // print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                
                role.removeAll()
                Rolelist.removeAll()
                for i in myresult! {
                    let id = i["Role_Id"].intValue
                    let roleName = i["Role_Name"].stringValue

                    self.role.append(Role(Role_Id: id, Role_Name: roleName))
                    self.Rolelist.append(roleName)
                    self.roleId.append(id)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    

    
    
    var newWatch = [NewWatchlist]()
    var newlist: [String] = []
    var newWatchId: [Int] = []
    
    func fetchNewWatchlistApi(){
        var id = 0
        // movie.removeAll()
         if usrlogin != nil {
             id = usrlogin!.UId
         }
        let url = "http://10.211.55.3/replicaimdbapi/api/Movie/NewWatchList?id=\(id)"
        AF.request(url, method: .get).responseJSON { [self]
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
               // print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                
                newWatch.removeAll()
                newlist.removeAll()
                for i in myresult! {
                    let id = i["W_Id"].intValue
                    let roleName = i["W_Name"].stringValue

                    self.newWatch.append(NewWatchlist(W_Id: id, W_Name: roleName))
                    self.newlist.append(roleName)
                    self.newWatchId.append(id)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    
    
}
