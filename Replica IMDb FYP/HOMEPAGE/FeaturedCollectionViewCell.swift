//
//  FeaturedCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 26/03/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class FeaturedCollectionViewCell: UICollectionViewCell {
    
    var delegate: FeatureCollectionDelegate?
    var fadingLabel: UILabel!
    var index = 0
    var movieId = 0
    @IBOutlet weak var FeaturedImageView: UIImageView!
    @IBOutlet weak var FeaturedVideosNameLbl: UILabel!
    @IBOutlet weak var Addbtn: UIButton!
    @IBOutlet weak var AddTag: UIImageView!
    
    override func awakeFromNib() {
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeaturedCollectionViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
        ///***********************************************************

             // Fading Label Working
        fadingLabel = UILabel()
        fadingLabel.text = "Text"
        fadingLabel.textColor = .black
        contentView.addSubview(fadingLabel)
        fadingLabel.isHidden = true
        fadingLabel.translatesAutoresizingMaskIntoConstraints = false
        fadingLabel.topAnchor.constraint    (equalTo: contentView.topAnchor,   constant:     100).isActive = true
        fadingLabel.leftAnchor.constraint   (equalTo: contentView.leftAnchor,  constant:     30).isActive = true
        fadingLabel.widthAnchor.constraint  (equalTo: contentView.widthAnchor, multiplier:  0.7).isActive = true
        fadingLabel.heightAnchor.constraint (equalToConstant: 30).isActive = true
        
        ///***********************************************************
                //Cell designing
        
        self.contentView.layer.cornerRadius = 7.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        FeaturedImageView.layer.cornerRadius = 7
        
        
        //End of ViewDidLoad
    }
    ///***********************************************************

    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.FeatureCollectionDelegate(mIndex: index)
        }
    
    @objc private func btnAction() {
            print("did tap image view")
        delegate?.featuredBtnAction()
     //   delegate?.choseWatchlistData()
        }
    @objc private func ChoseRole() {
            print("did tap image view")
        //delegate?.choseWatchlistData()
        let vc = homeTab?.storyboard?.instantiateViewController(identifier: "ChoseRole") as! ChoseRoleViewController
                   // self.navigationController?.pushViewController(vc, animated: true)
               vc.modalPresentationStyle = .popover
               vc.popoverPresentationController?.delegate = homeTab
               vc.popoverPresentationController?.sourceRect = .zero
        vc.Source = self
        homeTab?.navigationController?.present(vc, animated: true, completion: {
            
        })
        }
    ///************************************************************
//MARK: API Working
    
    
    var watch = [Watchlist]()
    
    func InsertDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)&rId=\(RoleId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        I
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
                self.delegate?.fetchWatchlistData()
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            
            self.delegate?.fetchWatchlistData()
        }
    }
    
    
    @objc func functionName (notification: NSNotification){
        WatchlistForLogin()
    }
    
    
    
    var watch1 = [Watchlist]()
    
    func InsertDataInApi1(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoGenreWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)&wid=\(WatchId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        I
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
                self.delegate?.fetchNewWatchlistData()
            case .failure(_):
                print(Error.self)
            }
        }
    }
    var watchlist2 = [Movie]()
    func DeleteWatchlistData2(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoGenreWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&wid=\(WatchId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            
            self.delegate?.fetchWatchlistData()
        }
    }
    ///************************************************************
            //MARK: Watchlist Working
    @IBAction func AddWatchlist(_ sender: Any) {
        
        if usrlogin != nil{
         ChoseRole()
        }else{
            btnAction()
            
        }
        
        
        
    }
    
    
    func WatchlistForLogin() {
        
        
        if Addbtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            Addbtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            Addbtn.tintColor = .black
            Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            Addbtn.tag = 2
            print(Addbtn.tag)
            fadeMessage(message: "Added to Watchlist!", color: .black, finalAlpha: 0.0)
            InsertDataInApi1()
        }else{
            if Addbtn.tag == 2 {

                AddTag.tintColor = .black
                Addbtn.setImage(UIImage(systemName: "plus"), for: .normal)
                Addbtn.tintColor = .white
                Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)

                Addbtn.tag = 0
                print(Addbtn.tag)
                fadeMessage(message: "Removed to Watchlist!", color: .black, finalAlpha: 0.0)
                DeleteWatchlistData2()
            }
        }
    }
    ///***********************************************************
    //MARK: Fading Label
    func fadeMessage(message: String, color: UIColor, finalAlpha: CGFloat) {
        fadingLabel.text          = message
        fadingLabel.textColor = .black
        fadingLabel.alpha         = 1.0
        fadingLabel.isHidden      = false
        fadingLabel.textAlignment = .center
        fadingLabel.backgroundColor     = .white
        fadingLabel.layer.cornerRadius  = 5
        fadingLabel.layer.masksToBounds = true
        fadingLabel.font = .boldSystemFont(ofSize: 12)
        UIView.animate(withDuration: 3.0, animations: { () -> Void in
            self.fadingLabel.alpha = finalAlpha
        })
    }
    
}
///***********************************************************

protocol FeatureCollectionDelegate {
    func FeatureCollectionDelegate(mIndex: Int)
    func featuredBtnAction()
    func fetchWatchlistData()
    func choseWatchlistData()
    func fetchNewWatchlistData()
}
