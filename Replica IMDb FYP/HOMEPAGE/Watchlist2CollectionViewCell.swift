//
//  Watchlist2CollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 07/04/2021.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON

class Watchlist2CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var WatchlistImageView: UIImageView!
    @IBOutlet weak var WatchlistRating: CosmosView!
    @IBOutlet weak var WatchlistVideoNameLbl: UILabel!
    @IBOutlet weak var WatchlistBtn: UIButton!
    var fadingLabel: UILabel!
    var movieId = 0
    var delegate : watchlist?
    
    override func awakeFromNib() {
        //Cell designing
        self.contentView.layer.cornerRadius = 7.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        WatchlistImageView.layer.cornerRadius = 7
        
        // Fading Label Working
   fadingLabel = UILabel()
   fadingLabel.text = "Text"
   fadingLabel.textColor = .black
   contentView.addSubview(fadingLabel)
   fadingLabel.isHidden = true
   fadingLabel.translatesAutoresizingMaskIntoConstraints = false
        fadingLabel.topAnchor.constraint    (equalTo: contentView.topAnchor,   constant:     150).isActive = true
        fadingLabel.leftAnchor.constraint   (equalTo: contentView.leftAnchor,  constant:     3).isActive = true
        fadingLabel.widthAnchor.constraint  (equalTo: contentView.widthAnchor, multiplier:  0.95).isActive = true
        fadingLabel.heightAnchor.constraint (equalToConstant: 30).isActive = true
        
    }
    ///************************************************************
//MARK: API Working
    
    var watch = [Watchlist]()
    
    func InsertDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        ]
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
                self.delegate?.fetchWatchlistData()
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            self.delegate?.fetchWatchlistData()
        }
    }
    func DeleteWatchlistData2(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoGenreWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&wid=\(WatchId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            
            self.delegate?.fetchNewWatchlistData()
        }
    }
    
    
    
    @IBAction func WatchlistTagBtn(_ sender: Any) {
        fadeMessage(message: "Removed to Watchlist!", color: .black, finalAlpha: 0.0)
        DeleteWatchlistData2()
    }
    
    @IBAction func WatchlistedBtn(_ sender: Any) {
        fadeMessage(message: "Removed to Watchlist!", color: .black, finalAlpha: 0.0)
        DeleteWatchlistData2()
    }
    ///***********************************************************
    //MARK: Fading Label
    func fadeMessage(message: String, color: UIColor, finalAlpha: CGFloat) {
        fadingLabel.text          = message
        fadingLabel.textColor = .black
        fadingLabel.alpha         = 1.0
        fadingLabel.isHidden      = false
        fadingLabel.textAlignment = .center
        fadingLabel.backgroundColor     = .white
        fadingLabel.layer.cornerRadius  = 5
        fadingLabel.layer.masksToBounds = true
        fadingLabel.font = .boldSystemFont(ofSize: 12)
        UIView.animate(withDuration: 3.0, animations: { () -> Void in
            self.fadingLabel.alpha = finalAlpha
        })
    }
    
    
    
}
protocol watchlist {
    func fetchWatchlistData()
    func fetchNewWatchlistData()
    
}
