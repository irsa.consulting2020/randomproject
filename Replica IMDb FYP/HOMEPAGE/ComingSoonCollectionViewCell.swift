//
//  ComingSoonCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 26/03/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class ComingSoonCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var ComingSoonImageView: UIImageView!
    @IBOutlet weak var ComingSoonVideoNameLbl: UILabel!
    @IBOutlet weak var AddTag: UIImageView!
    @IBOutlet weak var AddBtn: UIButton!
    
    var fadingLabel: UILabel!
    var index = 0
    var delegate: CoomingCollectionDelegate?
    var movieId = 0
    
    override func awakeFromNib() {
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ComingSoonCollectionViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
        
        
        ///***********************************************************
        // Fading Label Working
        fadingLabel = UILabel()
        fadingLabel.text = "Text"
        fadingLabel.textColor = .black
        contentView.addSubview(fadingLabel)
        fadingLabel.isHidden = true
        fadingLabel.translatesAutoresizingMaskIntoConstraints = false
        fadingLabel.topAnchor.constraint    (equalTo: contentView.topAnchor,   constant:     100).isActive = true
        fadingLabel.leftAnchor.constraint   (equalTo: contentView.leftAnchor,  constant:     30).isActive = true
        fadingLabel.widthAnchor.constraint  (equalTo: contentView.widthAnchor, multiplier:  0.7).isActive = true
        fadingLabel.heightAnchor.constraint (equalToConstant: 30).isActive = true

        ///***********************************************************
                //cell designing
        self.contentView.layer.cornerRadius = 7.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        ComingSoonImageView.layer.cornerRadius = 7
        
        
        
       // End ViewDidLoad
    }
    ///************************************************************
//MARK: API Working
    
    var watch = [Watchlist]()
    
    func InsertDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)&rId=\(RoleId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        ]
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
                self.delegate?.fetchWatchlistData()
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            self.delegate?.fetchWatchlistData()
        }
    }
    
    
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.CoomingCollectionDelegate(mIndex: index)
        }
    @objc private func btnAction(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.CoomingBtnAction()
        }
    
    @IBOutlet weak var AddWatchlist: UIImageView!
    ///***********************************************************
        //MARK: Watchlist Working
    @IBAction func Addwatchlist(_ sender: Any) {
        if usrlogin != nil{
        if AddBtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            AddBtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            AddBtn.tintColor = .black
            AddBtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            AddBtn.tag = 2
            print(AddBtn.tag)
            fadeMessage(message: "Added to Watchlist!", color: .black, finalAlpha: 0.0)
            InsertDataInApi()
        }else{
            if AddBtn.tag == 2 {
               
                AddTag.tintColor = .black
                AddBtn.setImage(UIImage(systemName: "plus"), for: .normal)
                AddBtn.tintColor = .white
                AddBtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
                AddBtn.tag = 0
                print(AddBtn.tag)
                fadeMessage(message: "Removed to Watchlist!", color: .black, finalAlpha: 0.0)
                DeleteWatchlistData()
            }
          }
        }else{
            AddBtn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        }
    }
    ///***********************************************************
        //MARK: Fading Label
    func fadeMessage(message: String, color: UIColor, finalAlpha: CGFloat) {
        fadingLabel.text          = message
        fadingLabel.textColor = .black
        fadingLabel.alpha         = 1.0
        fadingLabel.isHidden      = false
        fadingLabel.textAlignment = .center
        fadingLabel.backgroundColor     = .white
        fadingLabel.layer.cornerRadius  = 5
        fadingLabel.layer.masksToBounds = true
        fadingLabel.font = .boldSystemFont(ofSize: 12)
        UIView.animate(withDuration: 3.0, animations: { () -> Void in
            self.fadingLabel.alpha = finalAlpha
        })
    }
    
    //End of class
}
///***********************************************************

protocol CoomingCollectionDelegate {
    func CoomingCollectionDelegate(mIndex: Int)
    func CoomingBtnAction()
    func fetchWatchlistData()
}
