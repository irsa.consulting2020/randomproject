//
//  AddNewWatchlistViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 10/07/2021.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

var GenreId = 0

class AddNewWatchlistViewController: UIViewController {

    @IBOutlet weak var WatchlistView: UIView!
    
    @IBOutlet weak var WatchlistTextField: UITextField!
    @IBOutlet weak var SubmitBtn: UIButton!
    
    var dropDown = DropDown()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        WatchlistView.layer.cornerRadius = 7
        SubmitBtn.layer.cornerRadius = 5
        
        
        fetchGenresapi()
        
    }
    

    @IBAction func SubmitBtn(_ sender: Any) {
        
        PostDataInApi()
        dismiss(animated: true, completion: nil)
        
    }
   
    @IBAction func dropdown(_ sender: Any) {
        dropDown.dataSource = list
                      dropDown.anchorView = (sender as! AnchorView)
                      dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
                          dropDown.show()
                          dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                              (sender as AnyObject).setTitle(item, for: .normal)
                           
                            self!.gId = self!.category1[index].Genres_Id
                            GenreId = self!.gId
                        //      self!.fetchNewWatchlistData()
                            
                              
                          }
    }
    var gId = 0
    
    func PostDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AddWatchlist")!
        let para: [String:Any] = [
            "W_Name": self.WatchlistTextField.text!,
            "UId": usrlogin?.UId,
            "Genres_Id": GenreId
        ]
        AF.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                homeTab?.fetchNewWatchlistApi()
                print("success")
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    
    var category1 = [Genres]()
    var list: [String] = []
    func fetchGenresapi(){
        let url = "http://10.211.55.3/replicaimdbapi/api/Genres/AllGenres"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
               // print(myresponse)
               // print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                for i in myresult! {
                    let id = i["Genres_Id"].intValue
                    let genres = i["Genres_Name"].stringValue

                    self.category1.append(Genres(Genres_Id: id, Genres_Name: genres))
                    self.list.append(genres)
                    //print(self.category1)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }

}
