//
//  AddPersonViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 20/06/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddPersonViewController: UIViewController {

    @IBOutlet weak var PopupView: UIView!
    @IBOutlet weak var NameTxtField: UITextField!
    @IBOutlet weak var SubmitBtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PopupView.layer.cornerRadius = 7
        SubmitBtn.layer.cornerRadius = 5
        
    }
    
    @IBAction func SubmitBTN(_ sender: Any) {
        PostDataInApi()
       
        dismiss(animated: true, completion: nil)
      //  let vc = storyboard?.instantiateViewController(identifier: "HomePage") as! HomeTabBarController
     //   vc.dropDown.reloadAllComponents()
       // self.navigationController?.pushViewController(vc, animated: true)
    //    self.navigationController?.popViewController(animated: true)
    }
    
    func PostDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AddRole")!
        let para: [String:Any] = [
            "Role_Name": self.NameTxtField.text!,
            "UId": usrlogin?.UId
        ]
        AF.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                homeTab?.fetchRoleApi()
                print("success")
            case .failure(_):
                print(Error.self)
            }
        }
    }

    

}
