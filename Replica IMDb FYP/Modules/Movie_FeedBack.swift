//
//  Movie_FeedBack.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 17/04/2021.
//

import Foundation

//   http://10.211.55.3/ReplicaIMDbAPI/api/FeedBack/Feedback

class FeedBack: Codable{
    
    var FId : Int = 0
    var UId : Int = 0
    var Movie_Id : Int = 0
    var User_Comment : String = ""
    var User_Rating : Float = 0
    
    internal init(FId: Int = 0, UId: Int = 0, Movie_Id: Int = 0, User_Comment: String = "", User_Rating: Float = 0) {
        self.FId = FId
        self.UId = UId
        self.Movie_Id = Movie_Id
        self.User_Comment = User_Comment
        self.User_Rating = User_Rating
    }

}
