//
//  Categories.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 12/04/2021.
//

import Foundation
import Alamofire


class Category {
    
    
    fileprivate var baseUrl = ""
    typealias CategoriesCallBack = (_ Categories:[Category]?, _ status: Bool, _ message: String) -> Void
    var callBack : CategoriesCallBack?
    
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func getAllCategoryName(endPoint: String){
        AF.request(self.baseUrl + endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response{
            (DataResponse) in print("we got the response")
            guard let data = DataResponse.data else {
                self.callBack?(nil, false, "")
                return
            }
            do{
                let category1 = try JSONDecoder().decode([Categories].self, from: data)
                print("Categories ==\(category1)")
                for cat in category1{
                    let CateId = cat["id"]
                    
                    
                    
                }
               // self.callBack?(category1, true, "")
            }catch{
              //  print("Error Decoding == \(error)")
                self.callBack?(nil, false, error.localizedDescription)
            }
            
        }
        
    }
    
    
    
    func completionHandler(callBack: @escaping CategoriesCallBack){
        self.callBack = callBack
    }
    
}



