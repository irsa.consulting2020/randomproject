//
//  Type.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 16/04/2021.
//

import Foundation


//Type:    http://10.211.55.3/ReplicaIMDbAPI/api/type/alltypes

struct Type: Decodable{
    
    var Type_Id: Int = 0
    var Type_Name: String = ""
    
    
    internal init(Type_Id: Int = 0, Type_Name: String = "") {
        self.Type_Id = Type_Id
        self.Type_Name = Type_Name
    }
    
    
}
