//
//  Movie.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 12/04/2021.
//

import Foundation

//MARK: Movie Class

// http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AllMovies

class Movie:Decodable{
    
   
    var Movie_Id : Int = 0
    var Movie_Name : String = ""
    var Movie_Path : String = ""
    var Movie_Time : String = ""
    var Movie_ReleaseDate : String = ""
    var Movie_Image : String = ""
    var Movie_Description : String = ""
    var Movie_Rating : Double = 0.0
    var Movie_Status : String = ""
    
    
    
    internal init(Movie_Id: Int = 0, Movie_Name: String = "", Movie_Path: String = "", Movie_Time: String = "", Movie_ReleaseDate: String = "", Movie_Image: String = "", Movie_Description: String = "", Movie_Rating: Double = 0.0, Movie_Status : String = "") {
        self.Movie_Id = Movie_Id
        self.Movie_Name = Movie_Name
        self.Movie_Path = Movie_Path
        self.Movie_Time = Movie_Time
        self.Movie_ReleaseDate = Movie_ReleaseDate
        self.Movie_Image = Movie_Image
        self.Movie_Description = Movie_Description
        self.Movie_Rating = Movie_Rating
        self.Movie_Status = Movie_Status
    }
    
    
}
