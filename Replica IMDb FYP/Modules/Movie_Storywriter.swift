//
//  Movie_Storywriter.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 16/04/2021.
//

import Foundation

//  http://10.211.55.3/ReplicaIMDbAPI/api/MovieStorywriter/AllMovieStorywriter

class Movie_Storywriter: Decodable {
    
    var Storywriter_Id : Int = 0
    var Movie_Id : Int = 0
    var Storywriter_Name : String = ""
    var Storywriter_Image : String = ""
    var Storywriter_Description : String = ""
    var Storywriter_KnownFor : String = ""
    var Storywriter_Award : String = ""

    internal init(Storywriter_Id: Int = 0, Movie_Id: Int = 0, Storywriter_Name: String = "", Storywriter_Image: String = "", Storywriter_Description: String = "", Storywriter_KnownFor: String = "", Storywriter_Award: String = "") {
        self.Storywriter_Id = Storywriter_Id
        self.Movie_Id = Movie_Id
        self.Storywriter_Name = Storywriter_Name
        self.Storywriter_Image = Storywriter_Image
        self.Storywriter_Description = Storywriter_Description
        self.Storywriter_KnownFor = Storywriter_KnownFor
        self.Storywriter_Award = Storywriter_Award
    }
}

class StoryWriter_Picture: Decodable {
    
    var S_Id : Int = 0
    var Storywriter_Id : Int = 0
    var Movie_Id : Int = 0
    var S_Pictures : String = ""
    
    internal init(S_Id: Int = 0, Storywriter_Id: Int = 0, Movie_Id: Int = 0, S_Pictures: String = "") {
        self.S_Id = S_Id
        self.Storywriter_Id = Storywriter_Id
        self.Movie_Id = Movie_Id
        self.S_Pictures = S_Pictures
    }
}
