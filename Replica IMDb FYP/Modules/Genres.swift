//
//  Category.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 12/04/2021.
//

import Foundation

//MARK: Category Class


//Genres:   http://10.211.55.3/replicaimdbapi/api/Genres/AllGenres
//Type:    http://10.211.55.3/ReplicaIMDbAPI/api/type/alltypes

class Genres: Decodable{
    
    var Genres_Id: Int = 0
    var Genres_Name: String = ""
    
    
    internal init(Genres_Id: Int = 0, Genres_Name: String = "") {
        self.Genres_Id = Genres_Id
        self.Genres_Name = Genres_Name
    }
    
    
    
}


//class Category {
//
//
//    fileprivate var baseUrl = ""
//    typealias CategoriesCallBack = (_ Categories:[Category]?, _ status: Bool, _ message: String) -> Void
//    var callBack : CategoriesCallBack?
//
//
//    init(baseUrl: String) {
//        self.baseUrl = baseUrl
//    }
//
//    func getAllCategoryName(endPoint: String){
//        AF.request(self.baseUrl + endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response{
//            (DataResponse) in print("we got the response")
//            guard let data = DataResponse.data else {
//                self.callBack?(nil, false, "")
//                return
//            }
//            do{
//                let category1 = try JSONDecoder().decode([Categories].self, from: data)
//                print("Categories ==\(category1)")
//
//               // self.callBack?(category1, true, "")
//            }catch{
//              //  print("Error Decoding == \(error)")
//                self.callBack?(nil, false, error.localizedDescription)
//            }
//
//        }
//
//    }
//
//
//
//    func completionHandler(callBack: @escaping CategoriesCallBack){
//        self.callBack = callBack
//    }
//
//}
