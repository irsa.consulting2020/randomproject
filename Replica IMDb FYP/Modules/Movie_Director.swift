//
//  Movie_Director.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 16/04/2021.
//

import Foundation

//   http://10.211.55.3/ReplicaIMDbAPI/api/MovieDirector/AllMovieDirector

class Movie_Director: Decodable {
    
    var Director_Id : Int = 0
    var Movie_Id : Int = 0
    var Director_Name : String = ""
    var Director_Image : String = ""
    var Director_Description : String = ""
    var Director_KnownFor : String = ""
    var Director_Award : String = ""
    
    internal init(Director_Id: Int = 0, Movie_Id: Int = 0, Director_Name: String = "", Director_Image: String = "", Director_Description: String = "", Director_KnownFor: String = "", Director_Award: String = "") {
        self.Director_Id = Director_Id
        self.Movie_Id = Movie_Id
        self.Director_Name = Director_Name
        self.Director_Image = Director_Image
        self.Director_Description = Director_Description
        self.Director_KnownFor = Director_KnownFor
        self.Director_Award = Director_Award
    }
}
class Director_Picture: Decodable {
    
    var D_Id : Int = 0
    var Director_Id : Int = 0
    var Movie_Id : Int = 0
    var Pictures : String = ""
    
    internal init(D_Id: Int = 0, Director_Id: Int = 0, Movie_Id: Int = 0, Pictures: String = "") {
        self.D_Id = D_Id
        self.Director_Id = Director_Id
        self.Movie_Id = Movie_Id
        self.Pictures = Pictures
    }
}
