//
//  Entities.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 22/03/2021.
//

import Foundation

//http://10.211.55.3/ReplicaIMDbAPI/api/User/AllUser

//MARK: USER CLASS

class User:Codable{
    
    var UId : Int = 0
    var UName : String = ""
    var UEmail : String = ""
    var UPassword : String = ""
    var UType : String = ""

    
    
    
    internal init(UId: Int = 0, UName: String = "", UEmail: String = "", UPassword: String = "", UType: String = "") {
        self.UId = UId
        self.UName = UName
        self.UEmail = UEmail
        self.UPassword = UPassword
        self.UType = UType
    }
    
    
}


