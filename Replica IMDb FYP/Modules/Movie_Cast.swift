//
//  Movie_Cast.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 16/04/2021.
//

import Foundation


//   http://10.211.55.3/ReplicaIMDbAPI/api/MovieCast/AllMovieCast

class Movie_Cast: Decodable {
    
    var Cast_Id : Int = 0
    var Movie_Id : Int = 0
    var Cast_Name : String = ""
    var Cast_Image : String = ""
    var Cast_DateofBirth : String = ""
    var Cast_City : String = ""
    var Cast_MoviesDone : Int = 0
    
    internal init(Cast_Id: Int = 0, Movie_Id: Int = 0, Cast_Name: String = "", Cast_Image: String = "", Cast_DateofBirth: String = "", Cast_City: String = "", Cast_MoviesDone: Int = 0) {
        self.Cast_Id = Cast_Id
        self.Movie_Id = Movie_Id
        self.Cast_Name = Cast_Name
        self.Cast_Image = Cast_Image
        self.Cast_DateofBirth = Cast_DateofBirth
        self.Cast_City = Cast_City
        self.Cast_MoviesDone = Cast_MoviesDone
    }
}
class Cast_Picture: Decodable {
    
    var C_Id : Int = 0
    var Cast_Id : Int = 0
    var Movie_Id : Int = 0
    var C_Pictures : String = ""
    
    internal init(C_Id: Int = 0, Cast_Id: Int = 0, Movie_Id: Int = 0, C_Pictures: String = "") {
        self.C_Id = C_Id
        self.Cast_Id = Cast_Id
        self.Movie_Id = Movie_Id
        self.C_Pictures = C_Pictures
    }
}
