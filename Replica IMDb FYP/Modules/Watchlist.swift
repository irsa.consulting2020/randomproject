//
//  Watchlist.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 27/05/2021.
//

import Foundation


class Watchlist: Codable {
    
    var id: Int = 0
    var Movie_Id: Int = 0
    var UId: Int = 0
    var Role_Id: Int = 0
    
    internal init(id: Int = 0, Movie_Id: Int = 0, UId: Int = 0, Role_Id: Int = 0) {
        self.id = id
        self.Movie_Id = Movie_Id
        self.UId = UId
        self.Role_Id = Role_Id
    }
}

class NewWatchlistData: Decodable {
    
    var id: Int = 0
    var Movie_Id: Int = 0
    var UId: Int = 0
    var W_Id: Int = 0
    
    internal init(id: Int = 0, Movie_Id: Int = 0, UId: Int = 0, W_Id: Int = 0) {
        self.id = id
        self.Movie_Id = Movie_Id
        self.UId = UId
        self.W_Id = W_Id
    }
}
