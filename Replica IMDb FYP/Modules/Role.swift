//
//  Role.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 20/06/2021.
//

import Foundation

class Role: Decodable {
    
    var Role_Id: Int = 0
    var Role_Name: String = ""
    var UId: Int = 0
    
    internal init(Role_Id: Int = 0, Role_Name: String = "", UId: Int = 0) {
        self.Role_Id = Role_Id
        self.Role_Name = Role_Name
        self.UId = UId
    }
}


class NewWatchlist: Decodable {
    internal init(W_Id: Int = 0, W_Name: String = "", UId: Int = 0, Genres_Id: Int = 0) {
        self.W_Id = W_Id
        self.W_Name = W_Name
        self.UId = UId
        self.Genres_Id = Genres_Id
    }
    
    
    var W_Id: Int = 0
    var W_Name: String = ""
    var UId: Int = 0
    var Genres_Id: Int = 0
    
    
}
