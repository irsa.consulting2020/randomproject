//
//  Movie_Producer.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 16/04/2021.
//

import Foundation

//  http://10.211.55.3/ReplicaIMDbAPI/api/MovieProducer/AllMovieProducer

class Movie_Producer: Decodable {
    
    var Producer_Id : Int = 0
    var Movie_Id : Int = 0
    var Producer_Name : String = ""
    var Producer_Image : String = ""
    var Producer_Desccription : String = ""
    var Producer_KnownFor : String = ""
    var Producer_Award : String = ""
    
    
    internal init(Producer_Id: Int = 0, Movie_Id: Int = 0, Producer_Name: String = "", Producer_Image: String = "", Producer_Desccription: String = "", Producer_KnownFor: String = "", Producer_Award: String = "") {
        self.Producer_Id = Producer_Id
        self.Movie_Id = Movie_Id
        self.Producer_Name = Producer_Name
        self.Producer_Image = Producer_Image
        self.Producer_Desccription = Producer_Desccription
        self.Producer_KnownFor = Producer_KnownFor
        self.Producer_Award = Producer_Award
    }    
}
class Producer_Picture: Decodable {
    
    var P_Id : Int = 0
    var Producer_Id : Int = 0
    var Movie_Id : Int = 0
    var Producer_Pictures1 : String = ""
    
    internal init(P_Id: Int = 0, Producer_Id: Int = 0, Movie_Id: Int = 0, Producer_Pictures1: String = "") {
        self.P_Id = P_Id
        self.Producer_Id = Producer_Id
        self.Movie_Id = Movie_Id
        self.Producer_Pictures1 = Producer_Pictures1
    }
}
