//
//  FanFavouriteTableViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 07/04/2021.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON

class FanFavouriteTableViewCell: UITableViewCell {

    @IBOutlet weak var VideImageView: UIImageView!
    @IBOutlet weak var RatingView: CosmosView!
    @IBOutlet weak var VideoNameLbl: UILabel!
    @IBOutlet weak var AddTag: UIImageView!
    @IBOutlet weak var Addbtn: UIButton!
    // stop adding lines of codes here
    var delegate: SeeAllTableView?
    var index = 0
    var movieId = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FanFavouriteTableViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
        
    }
    ///************************************************************
//MARK: API Working
    
    var watch = [Watchlist]()
    
    func InsertDataInApi(){
        
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/insertIntoWatchlist?uid=\(loginUser!.UId)&mid=\(movieId)&rId=\(RoleId)")!
//        let para: [String:Any] = [
//            "Movie_Id": movieId,
//            "UId": loginUser!.UId,
//
//        ]
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            (response) in
            switch response.result  {
            case .success(_):
           //     print(response)
                print("success")
                self.delegate?.fetchWatchlistData()
            case .failure(_):
                print(Error.self)
            }
        }
    }
    
    var watchlist = [Movie]()
    func DeleteWatchlistData(){
        let url = URL(string: "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/DeleteIntoWatchlist?uid=\(usrlogin!.UId)&mid=\(movieId)&rId=\(RoleId)")!
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON{ (response) in
           
            self.delegate?.fetchWatchlistData()
        }
    }
    
    
    
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.SeeAllTableView(index: index)
        }
    @objc private func btnAction(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.SeeAllBtnAction()
        }
    

    @IBAction func TagBtn(_ sender: Any) {
        
        print("Tapped")
       
        if usrlogin != nil{
        if Addbtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            Addbtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            Addbtn.tintColor = .black
            Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            Addbtn.tag = 2
            print(Addbtn.tag)
            InsertDataInApi()
        }else{
            if Addbtn.tag == 2 {
               
                AddTag.tintColor = .black
                Addbtn.setImage(UIImage(systemName: "plus"), for: .normal)
                Addbtn.tintColor = .white
                Addbtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
               
                Addbtn.tag = 0
                print(Addbtn.tag)
                DeleteWatchlistData()
            }
            
            
        }
        }else{
            Addbtn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        }
        
        
        
    }
    
    
}
protocol SeeAllTableView {
    func SeeAllTableView(index: Int)
    func SeeAllBtnAction()
    func fetchWatchlistData()
}
