//
//  VideoPlay2ViewController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 24/04/2021.
//

import UIKit
import AVKit
import AVFoundation
import Cosmos
class VideoPlay2ViewController: UIViewController {

    
    var MovieDetail: Movie? = nil
    
    @IBOutlet weak var Rating: CosmosView!
    @IBOutlet weak var VideoView: UIView!
    @IBOutlet weak var OfficialTrailer: UILabel!
    @IBOutlet weak var VideoDescription: UILabel!
    @IBOutlet weak var VideoName: UILabel!
    @IBOutlet weak var VideoImage: UIImageView!
    @IBOutlet weak var AddTag: UIImageView!
    @IBOutlet weak var AddBtn: UIButton!
    var player = AVPlayer()
    let avController = AVPlayerViewController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        videoplayer()
        videosData()
        
        VideoNameGesture()
        
        
       //PlayPauseWorking()
        
        
        //End Of ViewDidLoad
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player.pause()
      //  player = nil
    }
    @IBAction func AddWatchlist(_ sender: Any) {
        if AddBtn.tag == 0 {
            AddTag.tintColor = .systemYellow
            AddBtn.setImage(UIImage(systemName: "checkmark"), for: .normal)
            AddBtn.tintColor = .black
            AddBtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
            AddBtn.tag = 2
            print(AddBtn.tag)
        }else{
            if AddBtn.tag == 2 {
               
                AddTag.tintColor = .black
                AddBtn.setImage(UIImage(systemName: "plus"), for: .normal)
                AddBtn.tintColor = .white
                AddBtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
               
                AddBtn.tag = 0
                print(AddBtn.tag)
            }
        }
        
    }
    func videoplayer(){
        let url = MovieDetail?.Movie_Path
        
//        avController.showsPlaybackControls = true
//        avController.view.frame = VideoView.bounds
//        self.VideoView.addSubview(avController.view)
//        avController.player = player
        
        self.player = AVPlayer(url: URL(string: url!)!) // your video url
        let playerLayer = AVPlayerLayer(player: self.player)
        playerLayer.frame = self.VideoView.bounds
        self.VideoView.contentMode = .scaleAspectFill
        playerLayer.masksToBounds = true
        playerLayer.frame = self.VideoView.bounds
        playerLayer.frame = CGRect(x: 0, y: 0, width: 428, height: 230)
        self.view.layer.insertSublayer(playerLayer, at: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        player.seek(to: CMTime.zero)
        playerLayer.videoGravity = .resize
        self.VideoView.layer.addSublayer(playerLayer)
        //self.player.play()
        
    }
    @objc func playerItemDidReachEnd(){
        player.seek(to: CMTime.zero)
    }
    
    @IBAction func PlayBtn(_ sender: Any) {
        if player.timeControlStatus == .playing {
            player.pause()
        
        }else{
            if player.timeControlStatus == .paused {
                player.play()
            }
        }
    }
    func PlayPauseWorking(){
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.Play(_:)))
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.Pause(_:)))
        switch player.timeControlStatus {
        case .paused:
            tap1.numberOfTapsRequired = 1
            self.VideoView.isUserInteractionEnabled = true
            self.VideoView.addGestureRecognizer(tap1)

        case .playing:
            tap.numberOfTapsRequired = 1
            self.VideoView.isUserInteractionEnabled = true
            self.VideoView.addGestureRecognizer(tap)
        default:
            break
        }
       
    }
    
   @objc func Pause(_ tap: UITapGestureRecognizer){
    player.pause()
    print("Done")
    }
    @objc func Play(_ tap: UITapGestureRecognizer){
     player.play()
     print("Done")
     }
    func videosData(){
        
        let data = try! Data(contentsOf: URL(string: MovieDetail!.Movie_Image)!)
        self.VideoImage.image =  UIImage(data: data)
        self.VideoName.text = MovieDetail?.Movie_Name
        self.VideoDescription.text = MovieDetail?.Movie_Description
        self.Rating.text = "\(MovieDetail?.Movie_Rating)"
    }
   
///************************************************************************
    //MARK: HyperlinkGesture
    func VideoNameGesture() {

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.videoNameTapped(_:)))
        tap.numberOfTapsRequired = 1
        self.VideoName.isUserInteractionEnabled = true
        self.VideoName.addGestureRecognizer(tap)
        VideoName.textColor = .systemBlue
    }
    
    @objc func videoNameTapped(_ tap: UITapGestureRecognizer) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlayer") as! VideoPlayingViewController
        vc.MovieDetail = MovieDetail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
// End Of Class
}
