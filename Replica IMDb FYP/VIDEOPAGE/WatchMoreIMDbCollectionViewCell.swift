//
//  WatchMoreIMDbCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 27/03/2021.
//

import UIKit

class WatchMoreIMDbCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var WatchMoreIMDbImageView: UIImageView!
    
    @IBOutlet weak var WatchMoreIMDbVideoNameLbl: UILabel!
    
    override init(frame: CGRect) {
           super.init(frame: frame)


           layer.shadowColor = UIColor.white.cgColor
           layer.shadowOffset = CGSize(width: 0, height: 2.0)
           layer.shadowRadius = 5.0
           layer.shadowOpacity = 1.0
           layer.masksToBounds = false
           layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
           layer.backgroundColor = UIColor.clear.cgColor

           contentView.layer.masksToBounds = true
           layer.cornerRadius = 10
       }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    
}
