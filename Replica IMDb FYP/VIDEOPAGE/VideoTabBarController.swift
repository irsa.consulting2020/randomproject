//
//  VideoTabBarController.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 18/03/2021.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON

class VideoTabBarController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, popularCollectionDelegate, recentTrailersCollectionDelegate, interviewsCollectionDelegate, entertainmentCollectionDelegate, whatTowatchCollectionDelegate{

    
    //MARK: CollectionView Delegate
    
    func popularCollectionDelegate(index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlay") as! VideoPlay2ViewController
        vc.MovieDetail = movie[index]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
    func recentTrailersCollectionDelegate(index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlay") as! VideoPlay2ViewController
        vc.MovieDetail = movie[index]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
    func interviewsCollectionDelegate(index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlay") as! VideoPlay2ViewController
        vc.MovieDetail = movie[index]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
    func entertainmentCollectionDelegate(index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlay") as! VideoPlay2ViewController
        vc.MovieDetail = movie[index]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
    func whatTowatchCollectionDelegate(index: Int) {
        let vc = storyboard?.instantiateViewController(identifier: "VideoPlay") as! VideoPlay2ViewController
        vc.MovieDetail = movie[index]
        self.navigationController?.pushViewController(vc, animated: true)
        print("Tapped")
    }
    
    
///************************************************************************
    // Initializers
    
    internal init(PopularTrailersCollectionView: UICollectionView? = nil, RecentTrailersCollectionView: UICollectionView? = nil, InterviewsAndMoreCollectionView: UICollectionView? = nil, EntertainmentNewsCollectionView: UICollectionView? = nil, WhatToWatchCollectionView: UICollectionView? = nil, WatchMoreIMDbCollectionView: UICollectionView? = nil, GenresRecentBtn: UIButton? = nil, TypeRecentBtn: UIButton? = nil, GenresBtn: UIButton? = nil, TypeBtn: UIButton? = nil, popularLbl: UILabel? = nil, RecentLbl: UILabel? = nil, InterviewLbl: UILabel? = nil, EntertainmentLbl: UILabel? = nil, WatchLbl: UILabel? = nil, WatchIMDbLbl: UILabel? = nil, selectedButton: UIButton = UIButton(), dataSource: [String] = [String]()) {
        self.PopularTrailersCollectionView = PopularTrailersCollectionView
        self.RecentTrailersCollectionView = RecentTrailersCollectionView
        self.InterviewsAndMoreCollectionView = InterviewsAndMoreCollectionView
        self.EntertainmentNewsCollectionView = EntertainmentNewsCollectionView
        self.WhatToWatchCollectionView = WhatToWatchCollectionView
        self.GenresRecentBtn = GenresRecentBtn
        self.TypeRecentBtn = TypeRecentBtn
        self.GenresBtn = GenresBtn
        self.TypeBtn = TypeBtn
        self.popularLbl = popularLbl
        self.RecentLbl = RecentLbl
        self.InterviewLbl = InterviewLbl
        self.EntertainmentLbl = EntertainmentLbl
        self.WatchLbl = WatchLbl
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // fatalError("init(coder:) has not been implemented")
    }
    
   
///************************************************************************
    // Outlets For Collection Views
    
    @IBOutlet weak var PopularTrailersCollectionView: UICollectionView!
    @IBOutlet weak var RecentTrailersCollectionView: UICollectionView!
    @IBOutlet weak var InterviewsAndMoreCollectionView: UICollectionView!
    @IBOutlet weak var EntertainmentNewsCollectionView: UICollectionView!
    @IBOutlet weak var WhatToWatchCollectionView: UICollectionView!
    
///************************************************************************
    // Outlets
    
    @IBOutlet weak var GenresRecentBtn: UIButton!
    @IBOutlet weak var TypeRecentBtn: UIButton!
    @IBOutlet weak var GenresBtn: UIButton!
    @IBOutlet weak var TypeBtn: UIButton!
    @IBOutlet weak var popularLbl: UILabel!
    @IBOutlet weak var RecentLbl: UILabel!
    @IBOutlet weak var InterviewLbl: UILabel!
    @IBOutlet weak var EntertainmentLbl: UILabel!
    @IBOutlet weak var WatchLbl: UILabel!
    
    
///************************************************************************
    //MARK: Variable Declaration
    
    var dataSource = [String]()
    let dropDown = DropDown()
    
///************************************************************************
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
            
        //Designing Label and Buttons
        
        popularLbl.layer.cornerRadius = 4.0
        RecentLbl.layer.cornerRadius = 4.0
        InterviewLbl.layer.cornerRadius = 4.0
        EntertainmentLbl.layer.cornerRadius = 4.0
        WatchLbl.layer.cornerRadius = 4.0
        popularLbl.clipsToBounds = true
        RecentLbl.clipsToBounds = true
        InterviewLbl.clipsToBounds = true
        EntertainmentLbl.clipsToBounds = true
        WatchLbl.clipsToBounds = true
        TypeBtn.layer.cornerRadius = 20
        GenresBtn.layer.cornerRadius = 20
        TypeRecentBtn.layer.cornerRadius = 20
        GenresRecentBtn.layer.cornerRadius = 20

                  ///********************

        // COLLECTION VIEW'S WORKING
        
        PopularTrailersCollectionView.delegate = self
        PopularTrailersCollectionView.dataSource = self
        
        RecentTrailersCollectionView.delegate = self
        RecentTrailersCollectionView.dataSource = self
        
        InterviewsAndMoreCollectionView.delegate = self
        InterviewsAndMoreCollectionView.dataSource = self
        
        EntertainmentNewsCollectionView.delegate = self
        EntertainmentNewsCollectionView.dataSource = self
        
        WhatToWatchCollectionView.delegate = self
        WhatToWatchCollectionView.dataSource = self
        
                 ///********************

        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
     //   DropDown.appearance().selectedTextColor = UIColor.systemBlue
        
                 ///********************
        
        fetchTypeapi()
        fetchGenresapi()
        fetchCollectionViewApiData()
        
        
    }
    
///************************************************************************
    //MARK: API WORKING
    
    var movie = [Movie]()
    
    func fetchCollectionViewApiData(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/Movie/AllMovies"
        AF.request(url, method: .get, parameters: nil).responseJSON{
            (response) in
            let response = response.data!
            let JsonArray = JSON(response).arrayValue
//            print(JsonArray)
            for record in JsonArray{
                let Movie_Id = record["Movie_Id"].intValue
                let Movie_Name = record["Movie_Name"].stringValue
                let Movie_Path = record["Movie_Path"].stringValue
                let Movie_Time = record["Movie_Time"].stringValue
                let Movie_ReleaseDate = record["Movie_ReleaseDate"].stringValue
                let Movie_Image = record["Movie_Image"].stringValue
                let Movie_Description = record["Movie_Description"].stringValue
                let Movie_Rating = record["Movie_Rating"].doubleValue
                let Movie_Status = record["Movie_Status"].stringValue

                let movierecord = Movie(Movie_Id: Movie_Id,
                                        Movie_Name: Movie_Name,
                                        Movie_Path: Movie_Path,
                                        Movie_Time: Movie_Time,
                                        Movie_ReleaseDate: Movie_ReleaseDate,
                                        Movie_Image: Movie_Image,
                                        Movie_Description: Movie_Description,
                                        Movie_Rating: Movie_Rating,
                                        Movie_Status: Movie_Status)

                self.movie.append(movierecord)
            }
            self.PopularTrailersCollectionView.reloadData()
            self.RecentTrailersCollectionView.reloadData()
            self.EntertainmentNewsCollectionView.reloadData()
            self.InterviewsAndMoreCollectionView.reloadData()
            self.WhatToWatchCollectionView.reloadData()
        }
    }
    
    
///************************************************************************
    
    var category1 = [Genres]()
    var type = [Type]()
    var categorylist: [String] = []
    func fetchGenresapi(){
        let url = "http://10.211.55.3/replicaimdbapi/api/Genres/AllGenres"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
             //   print(myresponse)
            //    print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
            //    print(myresult!)
                for i in myresult! {
                    let id = i["Genres_Id"].intValue
                    let genres = i["Genres_Name"].stringValue

                    self.category1.append(Genres(Genres_Id: id, Genres_Name: genres))
                    self.categorylist.append(genres)
                   // print(self.category1)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    var typelist : [String] = []
    
    func fetchTypeapi(){
        let url = "http://10.211.55.3/ReplicaIMDbAPI/api/type/alltypes"
        AF.request(url, method: .get).responseJSON {
            (myresponse) in
            switch myresponse.result {
            case .success(_):
             //   print(myresponse)
            //    print("Success")
                let myresult = try? JSON(data: myresponse.data!).arrayValue
             //   print(myresult!)
                for i in myresult! {
                    let id = i["Type_Id"].intValue
                    let type = i["Type_Name"].stringValue

                    self.type.append(Type(Type_Id: id, Type_Name: type))
                    self.typelist.append(type)
                  //  print(self.category1)
                }
                self.dropDown.reloadAllComponents()
                break
            case .failure:
                print(myresponse.error!)
                break
            }
        }
    }
    
///************************************************************************
    //MARK: DropDown Actions
        
    @IBAction func didTapTypeBtn(_ sender: Any) {
        
        dropDown.dataSource = typelist //["All","Movies","TV Series"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
    }
    
    
    @IBAction func didTapGenresBtn(_ sender: Any) {
        
        dropDown.dataSource = categorylist //["All","Action","Adventure","Animation","Comedy","Crime","Drama","Fantasy","Horror","Thriller","Western"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
    }
    
    
    @IBAction func onClickRecentTypeBtn(_ sender: Any) {
        
        dropDown.dataSource = typelist //["All","Movies","TV Series"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
    }
    
    
    @IBAction func onClickRecentGenresBtn(_ sender: Any) {
        
        dropDown.dataSource = categorylist //["All","Action","Adventure","Animation","Comedy","Crime","Drama","Fantasy","Horror","Thriller","Western"]
        dropDown.anchorView = (sender as! AnchorView)
        dropDown.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
              guard let _ = self else { return }
                (sender as AnyObject).setTitle(item, for: .normal)
               
            }
    }
    
    
    
///************************************************************************
    //MARK: Collection Views
    
    var items = ["Fate","DragMeToHell","TheWitcher","Sabrina","theWorstWitch"]
    var videoaTitle = ["FateThe winx Saga","Drag me to hell","The Witcher","Chilling Adventures of Sabrina","The Worst Witch"]
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.PopularTrailersCollectionView:
            return self.movie.count
        case self.RecentTrailersCollectionView:
            return self.movie.count
        case self.InterviewsAndMoreCollectionView:
            return self.movie.count
        case self.EntertainmentNewsCollectionView:
            return self.movie.count
        case self.WhatToWatchCollectionView:
            return self.movie.count
        default:
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        switch collectionView {
        case self.PopularTrailersCollectionView:
            
            let cell: PopularTrailersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularTrailers", for: indexPath as IndexPath) as! PopularTrailersCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//         cell.PopularTrailersImageView.image = img
//         cell.PopularTrailersVideoNameLbl.text = videoaTitle[indexPath.row]
            
            cell.delegate = self
            cell.index = indexPath.row
            let data = movie[indexPath.row]
            cell.PopularTrailersVideoNameLbl.text = data.Movie_Name
            cell.PopularTimeLbl.text = data.Movie_Time
            AF.request( data.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell.PopularTrailersImageView.image = UIImage(data: responseData!, scale:1)

               case .failure(let error):
                   print("error--->",error)
               }
           }
           return cell
        case self.RecentTrailersCollectionView:
            
            let cell2: RecentTrailersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentTrailers", for: indexPath as IndexPath) as! RecentTrailersCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell2.RecentTrailersImageView.image = img
//            cell2.RecentTrailersVideoNameLbl.text = videoaTitle[indexPath.row]
            
            cell2.delegate = self
            cell2.index = indexPath.row
            let data = movie[indexPath.row]
            cell2.RecentTimeLbl.text = data.Movie_Time
            cell2.RecentTrailersVideoNameLbl.text = data.Movie_Name
            AF.request( data.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell2.RecentTrailersImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
           return cell2
        case self.InterviewsAndMoreCollectionView:
           
            let cell3: InterviewsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterviewsAndMore", for: indexPath as IndexPath) as! InterviewsCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell3.InterviewsImageView.image = img
//            cell3.InterviewsVideoNameLbl.text = videoaTitle[indexPath.row]
            
            cell3.delegate = self
            cell3.index = indexPath.row
            let data = movie[indexPath.row]
            cell3.InterViewsTimeLbl.text = data.Movie_Time
            cell3.InterviewsVideoNameLbl.text = data.Movie_Name
            AF.request( data.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell3.InterviewsImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
            return cell3
        case self.EntertainmentNewsCollectionView:
           
            let cell4: EntertainmentNewsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EntertainmentNews", for: indexPath as IndexPath) as! EntertainmentNewsCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell4.EntertainmentNewsImageView.image = img
//            cell4.EntertainmentNewsVideoNameLbl.text = videoaTitle[indexPath.row]
            
            cell4.delegate = self
            cell4.index = indexPath.row
            let data = movie[indexPath.row]
            cell4.EntertainmentNewsVideoNameLbl.text = data.Movie_Name
            cell4.EntertainmentTimeLbl.text = data.Movie_Time
            AF.request( data.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell4.EntertainmentNewsImageView.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
                return cell4
        case self.WhatToWatchCollectionView:
            
            let cell4: WhatToWatchCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WhatToWatch", for: indexPath as IndexPath) as! WhatToWatchCollectionViewCell

//         let img : UIImage = UIImage(named: items[indexPath.row])!
//            cell4.WhatToWatchImageViews.image = img
//            cell4.WhatToWatchVideoNameLbl.text = videoaTitle[indexPath.row]

            cell4.delegate = self
            cell4.index = indexPath.row
            let data = movie[indexPath.row]
            cell4.WhatToWatchTimeLbl.text = data.Movie_Time
            cell4.WhatToWatchVideoNameLbl.text = data.Movie_Name
            AF.request( data.Movie_Image, method: .get).response{ response in
              switch response.result {
               case .success(let responseData):
                cell4.WhatToWatchImageViews.image = UIImage(data: responseData!, scale:1)
               case .failure(let error):
                   print("error--->",error)
               }
           }
                return cell4
        default:
            return UICollectionViewCell()
    }
}
    
    
///************************************************************************
    //MARK: SeeAll Actions
    
    @IBAction func PopularVideo(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func RecentVideos(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func InterviewsVideo(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func EntertainmentVideos(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func WhatTowatchVideos(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "FanFav") as! FanFavouriteViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
}
