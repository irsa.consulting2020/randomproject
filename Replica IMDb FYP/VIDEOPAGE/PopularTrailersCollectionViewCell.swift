//
//  PopularTrailersCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 27/03/2021.
//

import UIKit

class PopularTrailersCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var PopularTrailersImageView: UIImageView!
    @IBOutlet weak var PopularTrailersVideoNameLbl: UILabel!
    @IBOutlet weak var PopularTimeLbl: UILabel!
    var delegate: popularCollectionDelegate?
    var index = 0
    
    
    override func awakeFromNib() {
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PopularTrailersCollectionViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
               
        
        self.contentView.layer.cornerRadius = 7.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        PopularTrailersImageView.layer.cornerRadius = 7
        
    }
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.popularCollectionDelegate(index: index)
        }
    
    
    
    
}
protocol popularCollectionDelegate {
    func popularCollectionDelegate(index: Int)
}
