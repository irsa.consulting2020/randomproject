//
//  InterviewsCollectionViewCell.swift
//  Replica IMDb FYP
//
//  Created by Macbook on 27/03/2021.
//

import UIKit

class InterviewsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var InterviewsImageView: UIImageView!
    @IBOutlet weak var InterviewsVideoNameLbl: UILabel!
    @IBOutlet weak var InterViewsTimeLbl: UILabel!
    var delegate: interviewsCollectionDelegate?
    var index = 0
    
    
    override func awakeFromNib() {
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InterviewsCollectionViewCell.didTap(_:)))
            addGestureRecognizer(tapGestureRecognizer)
         
        self.contentView.layer.cornerRadius = 7.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        InterviewsImageView.layer.cornerRadius = 7
        
        
        
    }
    
    @objc private func didTap(_ sender: UITapGestureRecognizer) {
            print("did tap image view")
        delegate?.interviewsCollectionDelegate(index: index)
        }
    
    
}
protocol interviewsCollectionDelegate {
    func interviewsCollectionDelegate(index: Int)
}
